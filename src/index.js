import React from "react";
import ReactDOM from "react-dom";
import RootApp from './RootApp';
import registerServiceWorker from './registerServiceWorker';
import {AppContainer} from 'react-hot-loader';
require('dotenv').config()

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <RootApp/>
    </AppContainer>,
    document.getElementById('root')
  );
};

registerServiceWorker();

render(RootApp);

if (module.hot) {
  module.hot.accept('./RootApp', () => {
    render(RootApp);
  });
}
