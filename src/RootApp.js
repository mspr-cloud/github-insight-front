import React from "react";
import {ConnectedRouter} from "react-router-redux";
import {Provider} from "react-redux";
import {Route, Switch, HashRouter} from "react-router-dom";

import "assets/vendors/style";
import "styles/index.less";
import configureStore, {history} from "./store";
import App from "./containers/App/index";


export const store = configureStore();

const Root = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <HashRouter>
        <Switch>
          <Route path="/" component={App}/>
        </Switch>
      </HashRouter>
    </ConnectedRouter>
  </Provider>;


export default Root;
