import React from "react";
import CustomScrollbars from 'util/CustomScrollbars'

import RepositoryItem from "./RepositoryItem";

const RepositoryList = (({repositories, onRepositorySelect}) => {
  return (
    <div className="gi-module-list gi-repository-list">
      <CustomScrollbars className="gi-module-content-scroll">
        {repositories.map((repository, index) =>
          <RepositoryItem key={index} index={index} repository={repository} onRepositorySelect={onRepositorySelect}
                    />
        )}
      </CustomScrollbars>
    </div>
  )
});

export default RepositoryList;
