import React from "react";
import {Avatar, Badge} from "antd";

import IntlMessages from "util/IntlMessages";
import moment from "moment";
import {Link} from "react-router-dom";


const RepositoryItem = (({repository, onRepositorySelect}) => {

  return (
    <Link to={`/organisation/repository/${repository.name}/view`} className={"gi-link-unstyled"}>
    <div className="gi-module-list-item gi-repository-cell">
      <div className="gi-module-list-icon">
        <div>
          {repository.totalStars} <i className="gi-text-orange icon icon-star"/>
        </div>

        <div className="gi-ml-2">
          <Avatar className="gi-avatar gi-bg-blue gi-size-40" alt={repository.name}
                  src={repository.owner.avatarUrl}/>
        </div>
      </div>

      <div className="gi-repository-list-info">

        <div className="gi-module-list-content">
          <div className="gi-repository-user-des">

            <span className="gi-sender-name">{repository.name}</span>


            <div className="gi-time">{moment(repository.createdAt).fromNow()}</div>
          </div>


          <div className="gi-message">
            <p className="gi-text-truncate">{repository.description}</p>

          </div>
          <div className="gi-labels">
            {
              repository.primaryLanguage ?
                <Badge className="gi-badge gi-text-white" count={repository.primaryLanguage.name} style={{backgroundColor: repository.primaryLanguage.color}}/>
                :
                <Badge className="gi-badge gi-bg-grey gi-text-white" count={<IntlMessages id="app.organisation.repositories.language.unknown"/>}/>
            }
          </div>
        </div>

      </div>
    </div>
    </Link>
  )
});

export default RepositoryItem;
