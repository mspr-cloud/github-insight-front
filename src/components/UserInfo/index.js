import React, {Component} from "react";
import {connect} from "react-redux";
import {Avatar, Popover} from "antd";
import {injectIntl} from "react-intl";
import IntlMessages from "util/IntlMessages";
import { logoutRequest } from 'store/ducks/Authentication';


class UserInfo extends Component {

  render() {

    const { lastName, firstName, disconnect } = this.props;

    const fullName = `${firstName} ${lastName}`
    const userMenuOptions = (
      <ul className="gi-user-popover">
        <li onClick={() => disconnect()}><IntlMessages id="app.userAuth.logout"/></li>
      </ul>
    );

    return (
      <Popover overlayClassName="gi-popover-horizantal" placement="bottomRight" content={userMenuOptions}
               trigger="click">
        <Avatar src={`https://ui-avatars.com/api/?background=000&color=FFF&name=${fullName}`}
                className="gi-avatar gi-pointer" alt=""/>
      </Popover>
    )

  }
}

const mapStateToProps = state => ({
  firstName: state.authentication.firstName,
  lastName: state.authentication.lastName,
});


const mapDispatchToProps = dispatch => ({
  disconnect: () => dispatch(logoutRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(UserInfo));
