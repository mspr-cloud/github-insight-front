import React from "react";

class AppModuleHeader extends React.Component {

  constructor() {
    super();
    this.state = {
      popoverOpen: false
    };
    this.toggle = this.toggle.bind(this);

  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }

  render() {
    const {placeholder, onChange, value} = this.props;

    return (
      <div className="gi-module-box-header-inner">
        <div
          className="gi-search-bar gi-lt-icon-search-bar-lg gi-module-search-bar gi-d-none gi-d-sm-block">
          <div className="gi-form-group">
            <input className="ant-input gi-border-0" type="search" placeholder={placeholder}
                   onChange={onChange}
                   value={value}/>
            <span className="gi-search-icon gi-pointer"><i className="icon icon-search"/></span>
          </div>
        </div>
      </div>
    )
  }
}

export default AppModuleHeader;

AppModuleHeader.defaultProps = {
  styleName: '',
  value: '',
  notification: true,
  apps: true
};
