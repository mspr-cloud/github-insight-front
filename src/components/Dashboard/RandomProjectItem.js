import React from "react";
import Auxiliary from "util/Auxiliary";
import { Tag } from "antd";
import IntlMessages from "util/IntlMessages";

const RandomProjectItem = ({data}) => {
  const {name, description, primaryLanguage} = data;
  return (

    <Auxiliary>
      <h2 className="h3 gi-mb-2">{name || ''}</h2>
      <p className="gi-text-grey">{description || ''}</p>
      <span className="gi-text-primary gi-mb-1 gi-font-weight-medium"><IntlMessages id="app.dashboard.randomProjects.language"/> : <Tag
        className="gi-bg-grey gi-text-grey gi-mr-0 gi-mb-0 gi-rounded-xxl gi-order-sm-2">
        {primaryLanguage ?
          primaryLanguage.name
          :
          <IntlMessages id="app.dashboard.randomProjects.language.unknown"/>
        }
        </Tag>
      </span>

    </Auxiliary>
  );
};

export default RandomProjectItem;
