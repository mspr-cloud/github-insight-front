import React from "react";
import {Avatar, Checkbox, Tag, Tooltip} from "antd";
import moment from 'moment';
import Aux from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";

const ProjectItem = ({data}) => {

  const {name, primaryLanguage, url, createdAt, owner} = data;

  return (
    <Aux>
      <div className="gi-media gi-task-list-item gi-flex-nowrap">
        <div className="gi-media-body gi-task-item-content">
          <a className="gi-task-item-content-left" href={url} target="_blank">
            <p
              className={`gi-text-truncate gi-mb-0 gi-text-hover`}>{name}</p>
          </a>
          <div className="gi-task-item-content-right">
            <Avatar className="gi-ml-sm-3 gi-size-30 gi-order-sm-3" src={owner.avatarUrl}/>
            <Tag

              className="gi-bg-grey gi-text-grey gi-ml-3 gi-mr-0 gi-mb-0 gi-rounded-xxl gi-order-sm-2">{primaryLanguage ?
              primaryLanguage.name
              :
              <IntlMessages id="app.dashboard.randomProjects.language.unknown"/>
            }</Tag>
            <ul className="gi-dot-list gi-mb-0 gi-order-sm-1 gi-ml-2">
            </ul>
            <span className="gi-fs-sm gi-text-grey gi-ml-3 gi-task-date gi-order-sm-4">{moment(createdAt).fromNow()}</span>
          </div>
        </div>
      </div>
    </Aux>
  );
};

export default ProjectItem;
