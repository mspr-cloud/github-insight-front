import React, {Fragment} from "react";
import {Button} from "antd";
import awardFirst from "../../assets/images/award_first.svg"
import awardSecond from "../../assets/images/award_second.svg"
import Widget from "components/Widget/index";
import _ from 'lodash';
import CircularProgress from 'components/CircularProgress'
import IntlMessages from "util/IntlMessages";

const getRankIcon = (rank) => {
  switch (rank) {
    case "first": return awardFirst;
    case "second": return awardSecond;
    default: return awardFirst;
  }
}

const getRankClass = (rank) => {
  let defaultClass = {
    separator: 'gi-bg-success-dark',
    title: 'gi-text-success-dark'
  }
  switch (rank) {
    case "first": return defaultClass;
    case "second": return {
      separator: 'gi-bg-sepia',
      title: 'gi-text-sepia'
    };
    default: return defaultClass;
  }
}

const getRankTrans = (rank) => {
  let defaultTrans = "app.dashboard.languageTrends.first";
  switch (rank) {
    case "first":
      return defaultTrans;
    case "second":
      return "app.dashboard.languageTrends.second"
    default:
      return defaultTrans;
  }
}

const getRankValue = (rank, organisationLanguageTrends) => {

  let result = [];

  _.forIn(organisationLanguageTrends.languages,(value, key) => {
    result.push({key, value})
  });

  result.sort((a, b) => a.value - b.value);

  if(result.length === 0) {
    return null;
  }

  switch (rank) {
    case "first": return result[result.length-1];
    case "second": return result[result.length-2];
    default: return result[result.length-1];
  }
}

const LanguageTrends = ({rank, organisationLanguageTrends}) => {
  const rankClass = getRankClass(rank);
  const rankTrans = getRankTrans(rank);
  return (
    <Widget styleName="gi-card-full gi-text-center">
      {
        organisationLanguageTrends.loading ?
          <CircularProgress className="gi-h-100 rbc-month-row"/>
          :
          <Fragment>
            <div className="gi-pt-4 gi-px-3">
              <div className={`gi-separator ${rankClass.separator}`}/>
              <h2 className={`gi-mb-4 ${rankClass.title}`}><IntlMessages id={rankTrans}/></h2>
              <img className="gi-mb-4 gi-img-fluid gi-width-100"
                   src={getRankIcon(rank)} alt='spoons'/>
            </div>
            <Button type="primary"
                    className="gi-mt-sm-4 gi-fs-xl gi-btn-block gi-mb-0 gi-text-uppercase gi-border-radius-top-left-0 gi-border-radius-top-right-0"
                    size="large" htmlType="submit" block>
              {
                getRankValue(rank, organisationLanguageTrends) ?
                  getRankValue(rank, organisationLanguageTrends).key
                  :
                  null
              }
            </Button>
          </Fragment>
      }

    </Widget>
  );
};

export default LanguageTrends;
