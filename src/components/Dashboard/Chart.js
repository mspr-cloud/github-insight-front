import React, {Fragment} from "react";
import CircularProgress from "components/CircularProgress/index";
import Widget from "components/Widget/index";
import PropTypes from "prop-types";

class Chart extends React.Component {


  static defaultProps = {
    isHide: false,
    children: '',
    chartProperties: {
      prize: '',
      title: '',
      bgColor: '',
      styleName: '',
      desc: '',
      percent: '',
      loading: true,
    }
  }

  state = {
    isHide: false
  }

  handleToggle() {
    this.setState((previousState) => ({
      isHide: !previousState.isHide,
    }));
  }

  render() {
    const {isHide} = this.state;
    const {chartProperties, children} = this.props;
    const {prize, title, styleName, desc, bgColor, percent, loading} = chartProperties;
    return (
      <Widget styleName={`gi-card-full`}>
        <div className={isHide===true ? `gi-fillchart gi-bg-${bgColor} gi-fillchart-nocontent` : `gi-fillchart gi-bg-${bgColor} gi-overlay-fillchart`}>
          { loading ?
            <CircularProgress className="center"/> :
            <Fragment>
              <div className="ant-card-head-title">{title}</div>
              {children}
              <div className="gi-fillchart-content">
                <div className="ant-card-head-title gi-mb-3">{title}</div>
                <h2 className="gi-mb-2 gi-fs-xxxl gi-font-weight-medium">{prize}</h2> {percent > 0}
                <p className="gi-mb-0 gi-fs-sm"><span className={`gi-font-weight-medium gi-fs-md gi-chart-${styleName}`}>{percent}
                  {percent > 0 ? <i className="icon icon-menu-up gi-fs-sm"/> : null}</span>{desc}</p>
              </div>
              <div className="gi-fillchart-btn-close" onClick={this.handleToggle.bind(this)}><i className="icon icon-close" /></div>
              <div className="gi-fillchart-btn" onClick={this.handleToggle.bind(this)}><i className={`icon icon-stats gi-fs-xxxl`}/>
              </div>
            </Fragment>
          }
        </div>
      </Widget>
    );
  }
};

export default Chart;
