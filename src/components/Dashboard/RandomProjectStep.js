import React, {Component, Fragment} from "react";
import {Col, Row} from "antd";
import Slider from "react-slick";
import CircularProgress from 'components/CircularProgress'
import RandomProjectItem from "./RandomProjectItem";
import Widget from "components/Widget/index";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

class RandomProjectStep extends Component {

  state = {
    image: '#',
    loading: false,
    enabled: false,
    index: 0
  }

  render() {

    const { randomProjects } = this.props;
    const { index, image, enabled } = this.state;
    const settings = {
      arrows: false,
      dots: true,
      infinite: true,
      speed: 500,
      marginLeft: 10,
      marginRight: 10,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    if(randomProjects.list.length > 0 && !enabled) {
      this.setState({
        image: randomProjects.list[0].owner.avatarUrl,
        enabled: true
      })
    }

    return (
      <Widget styleName="gi-card-full">
        <Row>
          {
            randomProjects.loading ?
              <CircularProgress className="rbc-month-row gi-h-100 gi-maxw60"/>
              :
              <Fragment>
                <Col xl={10} lg={8} md={8} sm={8} xs={24}>
                  <a
                    href={randomProjects.list.length > 0 ? randomProjects.list[index].url : '#' }
                    target="_blank">
                  <div className="gi-slick-slider-lt-thumb">
                    <img
                    className={this.state.loading ? 'fadeout gi-img-fluid' : 'fadein gi-img-fluid'}
                    src={image}
                    alt="..."/></div>
                  </a>
                </Col>
                <Col xl={14} lg={16} md={16} sm={16} xs={24}>
                  <Slider className="gi-slick-slider gi-slick-slider-dot-top" {...settings}
                          afterChange={(index) => {
                            this.setState({loading: false, image: randomProjects.list[index].owner.avatarUrl})
                          }}
                          beforeChange={(oldIndex, newIndex) => {
                            this.setState({
                              loading: true,
                              index: newIndex
                            });
                          }}>
                    {randomProjects.list.map((data, index) =>
                      <RandomProjectItem key={index} data={data}/>)
                    }
                  </Slider>
                </Col>
              </Fragment>
          }
        </Row>
      </Widget>
    );
  }
}

export default RandomProjectStep;
