import React from "react";

import Aux from "util/Auxiliary.js"
import CircularProgress from 'components/CircularProgress'
import WidgetHeader from "components/WidgetHeader/index";
import IntlMessages from "util/IntlMessages";

const UsersMostStars = ({usersMostStars}) => {

  return (
    <Aux>
      <WidgetHeader styleName="gi-flex-row" title={<IntlMessages id="app.dashboard.userMostStars.title"/>}/>
      <ul className="gi-agents-list">
        {
          usersMostStars.loading ?
            <CircularProgress className="rbc-month-row gi-h-100 gi-maxw60"/>
            :
            usersMostStars.list.map((user, index) =>
              <li key={index}>
                <div className="gi-profileon gi-pointer">
                  <a href={user.repository.url} target="_blank">
                  <div className="gi-profileon-thumb"><img alt="..." src={user.avatarUrl} className="gi-rounded-lg"/></div>
                  <div className="gi-profileon-content">
                    <h5 className="gi-mb-1 gi-text-truncate">{user.repository.name || ''}</h5>
                    <p className="gi-mb-0 gi-fs-sm gi-text-truncate"><i
                      className={`icon icon-star gi-text-orange gi-pr-1`}/> {user.repository.totalStars} <span
                      className="gi-px-sm-1">|</span> {user.repository.primaryLanguage ?
                      user.repository.primaryLanguage.name
                      :
                      <IntlMessages id="app.dashboard.randomProjects.language.unknown"/>
                    }
                    </p>
                  </div>
                  </a>
                </div>
              </li>
            )
        }
      </ul>
    </Aux>
  );
};

export default UsersMostStars;
