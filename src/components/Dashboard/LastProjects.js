import React from "react";
import {Tabs} from "antd";
import Widget from "components/Widget";
import CircularProgress from 'components/CircularProgress'
import ProjectItem from "./ProjectItem";
import IntlMessages from "util/IntlMessages";
import {injectIntl} from "react-intl";

const TabPane = Tabs.TabPane;

class LastProjects extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const { latestProjects } = this.props;

    return (
      <Widget title={<IntlMessages id="app.dashboard.lastProjects.title"/>} styleName="gi-card-tabs"
              extra={<i className="icon icon-search-new gi-pointer gi-fs-xxl gi-text-primary"/>}>
        <Tabs defaultActiveKey="1">
          <TabPane tab={<IntlMessages id="app.dashboard.lastProjects.tabs.organization"/>} key="1">
            {
              latestProjects.loading ?
                <CircularProgress className="gi-h-100"/>
                :
              latestProjects.organisation.map((project, index) =>
                <ProjectItem key={project._id} data={project} />)
            }
          </TabPane>
          <TabPane tab={<IntlMessages id="app.dashboard.lastProjects.tabs.user"/>} key="2">{
            latestProjects.loading ?
              <CircularProgress className="gi-h-100"/>
              :
              latestProjects.users.map((project, index) =>
              <ProjectItem key={project._id} data={project} />)
          }</TabPane>
        </Tabs>
      </Widget>
    );
  }
}

export default injectIntl(LastProjects)
