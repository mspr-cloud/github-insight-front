import React from "react";
import {Avatar} from "antd";
import IntlMessages from "util/IntlMessages";

const ProfileHeader = ({repository}) => {
  return (
    <div className="gi-profile-banner">
      <div className="gi-profile-container">
        <div className="gi-profile-banner-top">
          <div className="gi-profile-banner-top-left">
            <div className="gi-profile-banner-avatar">
              <Avatar className="gi-size-90" alt={repository.name} src={repository.owner.avatarUrl}/>
            </div>
            <div className="gi-profile-banner-avatar-info">
              <h2 className="gi-mb-2 gi-mb-sm-3 gi-fs-xxl gi-font-weight-light">{repository.name}</h2>
              <p className="gi-mb-0 gi-fs-lg">{repository.description}</p>
            </div>
          </div>
          <div className="gi-profile-banner-top-right">
            <ul className="gi-follower-list">
              <li>
                <span className="gi-follower-title gi-fs-lg gi-font-weight-medium">{repository.totalStars}</span>
                <span className="gi-fs-sm"><IntlMessages id="app.organisation.repository.header.stars"/></span></li>
              <li>
                <span className="gi-follower-title gi-fs-lg gi-font-weight-medium">{repository.forkCount}</span>
                <span className="gi-fs-sm"><IntlMessages id="app.organisation.repository.header.forks"/></span></li>
              <li>
                <span className="gi-follower-title gi-fs-lg gi-font-weight-medium">{repository.totalCommit}</span>
                <span className="gi-fs-sm"><IntlMessages id="app.organisation.repository.header.commits"/></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProfileHeader;
