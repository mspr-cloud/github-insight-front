import React, {Fragment} from "react";
import {Col, Row, Tag} from "antd";
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";
import moment from "moment";


const About = ({repository}) => {
  return (
    <Widget title={<IntlMessages id="app.organisation.repository.about.title"/>} styleName="gi-card-tabs gi-card-tabs-right gi-card-profile">
      <div className="gi-mb-2">
        <Row>
          <Col key={"birthday"} xl={8} lg={12} md={12} sm={12} xs={24}>
            <Fragment>
              <div className="gi-media gi-flex-nowrap gi-mt-3 gi-mt-lg-4 gi-mb-2">
                <div className="gi-mr-3">
                  <i className={`icon icon-birthday-new gi-fs-xlxl gi-text-orange`}/>
                </div>
                <div className="gi-media-body">
                  <h6 className="gi-mb-1 gi-text-grey"><IntlMessages id="app.organisation.repository.about.creationDate"/></h6>
                  <p className="gi-mb-0">{moment(repository.createdAt).fromNow()}</p>
                </div>
              </div>
            </Fragment>
          </Col>
          <Col key={"team"} xl={8} lg={12} md={12} sm={12} xs={24}>
            <Fragment>
              <div className="gi-media gi-flex-nowrap gi-mt-3 gi-mt-lg-4 gi-mb-2">
                <div className="gi-mr-3">
                  <i className={`icon icon-team gi-fs-xlxl gi-text-orange`}/>
                </div>
                <div className="gi-media-body">
                  <h6 className="gi-mb-1 gi-text-grey"><IntlMessages id="app.organisation.repository.about.contributors"/></h6>
                  <p className="gi-mb-0">{Object.keys(repository.userTotalCommits).length}</p>
                </div>
              </div>
            </Fragment>
          </Col>
          <Col key={"security"} xl={8} lg={12} md={12} sm={12} xs={24}>
            <Fragment>
              <div className="gi-media gi-flex-nowrap gi-mt-3 gi-mt-lg-4 gi-mb-2">
                <div className="gi-mr-3">
                  <i className={`icon icon-forgot-password gi-fs-xlxl gi-text-orange`}/>
                </div>
                <div className="gi-media-body">
                  <h6 className="gi-mb-1 gi-text-grey"><IntlMessages id="app.organisation.repository.about.security"/></h6>
                    <Tag
                      className={`ant-tag ant-tag-has-color  ${repository.isPrivate ? 'gi-bg-danger' : 'gi-bg-success'}`}>
                      <p className="gi-mb-0">
                      { repository.isPrivate ?
                        <IntlMessages id="app.organisation.repository.about.security.private"/>
                        :
                        <IntlMessages id="app.organisation.repository.about.security.public"/>
                      }
                      </p>
                    </Tag>
                </div>
              </div>
            </Fragment>
          </Col>
        </Row>
      </div>
    </Widget>
  );
}
export default About;
