import React from "react";
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";
import { ResponsiveCalendar } from '@nivo/calendar';
import {Select} from "antd";
import moment from 'moment';
import _ from "lodash";

const Option = Select.Option;

const formatCommits = (commits) => {
  let result = [];
  _.forIn(commits.map((commit) => moment(commit.committedDate).format('YYYY-MM-DD'))
    .reduce((acum,cur) => Object.assign(acum,{[cur]: (acum[cur] | 0)+1}),{}),(value, key) => {
    result.push({day: key, value})
  });

  return result
}

const GraphCommits = ({commits, start, end, onYearChange}) => {

  return (
    <Widget styleName="gi-card-profile">
      <div className="ant-card-head">
        <div className="ant-row-flex gi-px-4 gi-pt-4">
          <span className="ant-card-head-title gi-mb-2"><IntlMessages id="app.organisation.repository.graphCommits.title"/></span>
          <div className="gi-ml-auto ">
            <Select className="gi-mb-2 gi-select-sm gi-minw120" defaultValue={moment()} onChange={onYearChange}>
              <Option value={moment()}><IntlMessages id="app.organisation.repository.graphCommits.currentYear"/></Option>
              <Option value={moment().subtract(1, 'year')}><IntlMessages id="app.organisation.repository.graphCommits.lastYear"/></Option>
              <Option value={moment().subtract(2, 'year')}>{moment().subtract(2, 'year').fromNow()}</Option>
              <Option value={moment().subtract(3, 'year')}>{moment().subtract(3, 'year').fromNow()}</Option>
              <Option value={moment().subtract(4, 'year')}>{moment().subtract(4, 'year').fromNow()}</Option>
              <Option value={moment().subtract(5, 'year')}>{moment().subtract(5, 'year').fromNow()}</Option>
            </Select>
          </div>
        </div>
      </div>
      <div style={{height: '200px'}}>
        <ResponsiveCalendar
          data={formatCommits(commits) || []}
          from={start.format('YYYY-MM-DD')}
          to={end.format('YYYY-MM-DD')}
          emptyColor="#eeeeee"
          colors={[ '#61cdbb', '#97e3d5', '#e8c1a0', '#f47560' ]}
          margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
          yearSpacing={40}
          monthBorderColor="#ffffff"
          dayBorderWidth={2}
          dayBorderColor="#ffffff"
          legends={[
            {
              anchor: 'bottom-right',
              direction: 'row',
              translateY: 36,
              itemCount: 4,
              itemWidth: 42,
              itemHeight: 36,
              itemsSpacing: 14,
              itemDirection: 'right-to-left'
            }
          ]}
        />
      </div>
    </Widget>
  )
}


export default GraphCommits;
