import React from "react";
import { Icon } from 'antd';
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";

const Links = ({repository}) => {
  return (
    <Widget title={<IntlMessages id="app.organisation.repository.links.title"/>} styleName="gi-card-profile-sm">
      <div key={"github"} className="gi-media gi-align-items-center gi-flex-nowrap gi-pro-contact-list">
        <div className="gi-mr-3">
          <Icon className="gi-fs-xxl gi-text-grey" type="github" />
        </div>
        <div className="gi-media-body">
          <a target="_blank" href={`https://github.com/${repository.owner.login}`}>
            <span className="gi-mb-0 gi-text-grey gi-fs-sm"><IntlMessages id="app.organisation.repository.links.github.owner"/></span>
            <p className="gi-mb-0">{repository.owner.login}</p>
          </a>
        </div>
      </div>
      <div key={"project"} className="gi-media gi-align-items-center gi-flex-nowrap gi-pro-contact-list">
        <div className="gi-mr-3">
          <Icon className="gi-fs-xxl gi-text-grey" type="project" />
        </div>
        <div className="gi-media-body">
          <a target="_blank" href={repository.url}>
            <span className="gi-mb-0 gi-text-grey gi-fs-sm"><IntlMessages id="app.organisation.repository.links.github.project"/></span>
            <p className="gi-mb-0">{repository.name}</p>
          </a>
        </div>
      </div>
    </Widget>
  )
}

export default Links;
