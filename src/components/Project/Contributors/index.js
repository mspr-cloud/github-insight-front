import React, {Fragment} from "react";
import {Table} from "antd";
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";
import _ from 'lodash';

const columns = [
  {
    title: <IntlMessages id="app.organisation.repository.contributors.table.contributor"/>,
    dataIndex: 'image',
    render: (text, record) => {
      return <div className="gi-flex-row gi-align-items-center">
        <img className="gi-rounded-circle gi-size-30 gi-mr-2" src={record.avatarUrl} alt={record.login}/>
        <p className="gi-mb-0">{record.login}</p>
      </div>
    },
  },
  {
    title: <IntlMessages id="app.organisation.repository.contributors.table.commits"/>,
    dataIndex: 'transfer',
    render: (text, record) => {
      return <span className="gi-text-grey">{record.total}</span>
    },

  }

];

const Contributors = ({repository}) => {

  let data = [];
  _.forIn(repository.userTotalCommits,(value, key) => {
    data.push(value)
  });
  data = data.sort((a, b) => b.total - a.total)

  return (
    <Widget
      title={
        <h2 className="h4 gi-text-capitalize gi-mb-0"><IntlMessages id="app.organisation.repository.contributors.title"/></h2>
      }>
      <div className="gi-table-responsive">
        <Table className="gi-table-no-bordered" columns={columns} dataSource={data} pagination={false}
               size="small"/>
      </div>
    </Widget>
  );
}
export default Contributors;
