import React from "react";
import PropTypes from "prop-types";

const WidgetHeader = ({title, extra, styleName}) => {

  return (
    <h2 className={`gi-entry-title ${styleName}`}>
      {title}
      <span className="gi-text-primary gi-fs-md gi-pointer gi-ml-auto gi-d-none gi-d-sm-block">{extra}</span>
    </h2>
  )
};

WidgetHeader.defaultProps = {
  styleName: '',
};

WidgetHeader.propTypes = {
  title: PropTypes.node,
  extra: PropTypes.node,
};

export default WidgetHeader;
