
const hooks = {
    before: (request, state) => {
        if (request.url.match(/^\/api/)) {
            request.url = `${process.env.REACT_APP_API_SCHEME}://${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}${request.url}`
        }

        if (request.headers.Authorization === null) {
            //request.headers.Authorization = `JWT ${state.user.token}`
        }
    },
}

export default hooks
