import {applyMiddleware, compose, createStore} from "redux";
import reducers, { epic } from "./ducks/index";
import { createEpicMiddleware } from 'redux-observable'
import {routerMiddleware} from "react-router-redux";
import { createHashHistory } from 'history';
import apiMiddleware from 'store/middleware/apiMiddleware'
import xhook from 'xhook'
import hooks from './utils/hooks'
const history = createHashHistory();
const epicMiddleware = createEpicMiddleware();
const routeMiddleware = routerMiddleware(history);

const middlewares = [epicMiddleware, routeMiddleware, apiMiddleware()];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState) {
  const store = createStore(reducers, initialState,
    composeEnhancers(applyMiddleware(...middlewares)));

  // XHR request middlewares
  if (hooks.before) {
    xhook.before((request) => hooks.before(request, store.getState()))
  }

  if (hooks.after) {
    xhook.after((response) => hooks.after(response, store.getState()))
  }

  epicMiddleware.run(epic);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./ducks/index', () => {
      const nextRootReducer = require('./ducks/index');
      store.replaceReducer(nextRootReducer);
    });
  }
  return store;
}
export {history};
