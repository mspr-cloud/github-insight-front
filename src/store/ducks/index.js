import {combineReducers} from "redux";
import { combineEpics } from 'redux-observable';
import {routerReducer} from "react-router-redux";
import { reducer as authenticationReducer, epic as authenticationEpic } from './Authentication';
import { reducer as organisationReducer } from './Organisation';
import { reducer as settingsReducer} from './Settings';
import { reducer as usersReducer} from './Users';
import { reducer as statsReducer} from './Stats';
import { reducer as repositoriesReducer} from './Repositories';



const reducers = combineReducers({
  routing: routerReducer,
  authentication: authenticationReducer,
  settings: settingsReducer,
  users: usersReducer,
  organisation: organisationReducer,
  repositories: repositoriesReducer,
  stats: statsReducer,
});

export default reducers;

export const epic = combineEpics(
  authenticationEpic,
)
