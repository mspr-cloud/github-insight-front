import { ofType, combineEpics } from "redux-observable"
import { NEVER, of, from } from "rxjs";
import { ajax } from "rxjs/ajax"
import { replace } from "react-router-redux"
import { map, mergeMap, catchError } from "rxjs/operators"
import { login, register, getAccount, loginCheck } from "../services/authentication"
import { openNotificationByType } from "util/notification"
import { intl } from "util/IntlMessages";
import { ROLE_USER } from "../../constants/Roles";

export const LOCALSTORAGE_REFRESH_TOKEN = "@localstorage/refreshToken";

export const LOGIN_REQUEST = "authentication/LOGIN_REQUEST";
export const LOGIN_FAILURE = "authentication/LOGIN_FAILURE";
export const LOGIN_SUCCESS = "authentication/LOGIN_SUCCESS";

export const LOGIN_CHECK_REQUEST = "authentication/LOGIN_CHECK_REQUEST";
export const LOGIN_CHECK_FAILURE = "authentication/LOGIN_CHECK_FAILURE";
export const LOGIN_CHECK_SUCCESS = "authentication/LOGIN_CHECK_SUCCESS";

export const REGISTER_REQUEST = "authentication/REGISTER_REQUEST";
export const REGISTER_FAILURE = "authentication/REGISTER_FAILURE";
export const REGISTER_SUCCESS = "authentication/REGISTER_SUCCESS";

export const GET_ACCOUNT_REQUEST = "authentication/GET_ACCOUNT_REQUEST";
export const GET_ACCOUNT_FAILURE = "authentication/GET_ACCOUNT_FAILURE";
export const GET_ACCOUNT_SUCCESS = "authentication/GET_ACCOUNT_SUCCESS";

export const LOGOUT_REQUEST = "authentication/LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "authentication/LOGOUT_SUCCESS";

// Reducer
const INITIAL_STATE = {
    id: null,
    isLogged: false,
    authorized: false,
    token: null,
    email: null,
    role: null,
    firstName: null,
    lastName: null,
    loading: false,
    loadingCheck: false,
}

export function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
      case LOGIN_CHECK_REQUEST:
            return {
                ...state,
                loadingCheck: true
            }
        case GET_ACCOUNT_REQUEST:
        case REGISTER_REQUEST:
        case LOGIN_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case GET_ACCOUNT_SUCCESS:
            return {
                ...state,
                isLogged: true,
                loading: false,
                loadingCheck: false,
                id: action.payload.id,
                token: action.payload.token,
                email: action.payload.email,
                firstName: action.payload.firstName,
                lastName: action.payload.lastName,
                role: action.payload.role,
                authorized: !(action.payload.role === ROLE_USER)
            }
        case LOGIN_CHECK_FAILURE:
        case REGISTER_FAILURE:
        case GET_ACCOUNT_FAILURE:
        case LOGIN_FAILURE: {
            return {
                ...state,
                loading: false,
                loadingCheck: false,
            }
        }
        case LOGOUT_SUCCESS: {
            return {
                ...INITIAL_STATE
            }
        }
        default:
            return state
    }
}


export function loginRequest(payload) {

    return {
        type: LOGIN_REQUEST,
        payload
    }
}
export function loginSuccess(payload) {
    return {
        type: LOGIN_SUCCESS,
        payload
    }
}
export function loginFailure(payload) {
    return {
        type: LOGIN_FAILURE,
        payload
    }
}

export const logoutRequest = () => {
    localStorage.removeItem(LOCALSTORAGE_REFRESH_TOKEN);
    return {
        type: LOGOUT_SUCCESS,
    }
};

export function getAccountRequest (token) {
    return {
        type: GET_ACCOUNT_REQUEST,
        token
    }
}

export function getAccountSuccess (payload) {
    return {
        type: GET_ACCOUNT_SUCCESS,
        payload
    }
}

export function getAccountFailure (payload) {
    return {
        type: GET_ACCOUNT_FAILURE,
        payload
    }
}

export function registerRequest(payload) {
    return {
        type: REGISTER_REQUEST,
        payload
    }
}

export function registerSuccess(payload) {
    return {
        type: REGISTER_SUCCESS,
        payload
    }
}

export function registerFailure(payload) {
    return {
        type: REGISTER_FAILURE,
        payload
    }
}


export function loginCheckRequest(refreshToken) {
    return {
        type: LOGIN_CHECK_REQUEST,
        refreshToken
    }
}

export function loginCheckSuccess(payload) {
    return {
        type: LOGIN_CHECK_SUCCESS,
        payload
    }
}

export function loginCheckFailure(payload) {
    return {
        type: LOGIN_CHECK_FAILURE,
        payload
    }
}

export const epic = combineEpics(
    loginRequestEpic,
    loginCheckRequestEpic,
    registerRequestEpic,
    accountRequestEpic,
    loginFailureEpic,
    registerFailureEpic,
    getAccountFailureEpic,
    loginSuccessEpic,
    logoutSuccessEpic
)

export function loginRequestEpic(action$) {
    return action$.pipe(
        ofType(LOGIN_REQUEST),
        mergeMap(({payload}) =>
            ajax(
                login(payload)
            ).pipe(
                map((res) => {
                    localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, res.response.refreshToken)
                    return getAccountRequest(res.response.token);
                }),
                catchError((err) => of(loginFailure(err))),
            )
        )
    )
}

export function loginCheckRequestEpic(action$) {
    return action$.pipe(
        ofType(LOGIN_CHECK_REQUEST),
        mergeMap(({refreshToken}) =>
            ajax(
                loginCheck(refreshToken)
            ).pipe(
                map((res) => {
                    localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, res.response.refreshToken)
                    return getAccountRequest(res.response.token)
                }),
                catchError((err) => of(loginCheckFailure(err))),
            ),
        ),
    )
}

export function registerRequestEpic(action$) {
    return action$.pipe(
        ofType(REGISTER_REQUEST),
        mergeMap(({payload}) =>
            ajax(
                register(payload)
            ).pipe(
                map((res) => {
                    localStorage.setItem(LOCALSTORAGE_REFRESH_TOKEN, res.response.refreshToken)
                    return getAccountRequest(res.response.token);
                }),
                catchError((err) => of(registerFailure(err))),
            )
        )
    )
}

export function accountRequestEpic(action$) {
    return action$.pipe(
        ofType(GET_ACCOUNT_REQUEST),
        mergeMap(({token}) =>
            ajax(
                getAccount(token)
            ).pipe(
                map((res) => getAccountSuccess({token, ...res.response})),
                catchError((err) => of(getAccountFailure(err))),
            ),
        ),
    )
}


export function loginSuccessEpic(action$, state$) {
    return action$.pipe(
        ofType(GET_ACCOUNT_SUCCESS),
        mergeMap(() => {
            openNotificationByType("success", intl.formatMessage({ id: "app.userAuth.account.success.title" }),
                `${intl.formatMessage({ id: "app.userAuth.account.success.body" })} ${state$.value.authentication.firstName}`)
            if (state$.value.authentication.authorized) {
                return of(replace("/main/dashboard"));
            } else {
                return of(replace("/denied"));
            }
        })
    )
}

export function logoutSuccessEpic(action$, state$) {
    return action$.pipe(
        ofType(LOGOUT_SUCCESS),
        mergeMap(() => of(replace("/signin")))
    )
}


/****
 *
 * FAILURE
 *
 */

export function loginFailureEpic(action$) {
    return action$.pipe(
        ofType(LOGIN_FAILURE),
        mergeMap(() => {
            openNotificationByType("error", intl.formatMessage({ id: "app.userAuth.signIn.error.title" }), intl.formatMessage({ id: "app.userAuth.signIn.error.body" }))
            return NEVER
        }),
    )
}

export function getAccountFailureEpic(action$) {
    return action$.pipe(
        ofType(GET_ACCOUNT_FAILURE),
        mergeMap(() => {
            openNotificationByType("error", intl.formatMessage({ id: "app.userAuth.account.error.title" }), intl.formatMessage({ id: "app.userAuth.account.error.body" }))
            return NEVER
        }),
    )
}

export function registerFailureEpic(action$) {
    return action$.pipe(
        ofType(REGISTER_FAILURE),
        mergeMap(() => {
            openNotificationByType("error", intl.formatMessage({ id: "app.userAuth.signUp.error.title" }), intl.formatMessage({ id: "app.userAuth.signUp.error.body" }))
            return NEVER
        }),
    )
}
