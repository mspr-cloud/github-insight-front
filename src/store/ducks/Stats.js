import { ajax } from 'rxjs/ajax'
import {map, catchError} from 'rxjs/operators';
import {
  getStatsOrganisation,
  getStatsLatestProjects,
  getStatsRandomProjects,
  getStatsUsersMostStars,
  getOrganisationLanguageTrends
} from 'store/services/stats'

export const GET_STATS_ORGANISATION_REQUEST = 'stats/STATS_ORGANISATION_REQUEST';
export const GET_STATS_ORGANISATION_SUCCESS = 'stats/STATS_ORGANISATION_SUCCESS';
export const GET_STATS_ORGANISATION_FAILURE = 'stats/STATS_ORGANISATION_FAILURE';
export const GET_STATS_ORGANISATION_END =     'stats/STATS_ORGANISATION_END';

export const GET_STATS_LATEST_PROJECTS_REQUEST = 'stats/STATS_LATEST_PROJECTS_REQUEST';
export const GET_STATS_LATEST_PROJECTS_SUCCESS = 'stats/STATS_LATEST_PROJECTS_SUCCESS';
export const GET_STATS_LATEST_PROJECTS_FAILURE = 'stats/STATS_LATEST_PROJECTS_FAILURE';
export const GET_STATS_LATEST_PROJECTS_END =     'stats/STATS_LATEST_PROJECTS_END';

export const GET_STATS_RANDOM_PROJECTS_REQUEST = 'stats/STATS_RANDOM_PROJECTS_REQUEST';
export const GET_STATS_RANDOM_PROJECTS_SUCCESS = 'stats/STATS_RANDOM_PROJECTS_SUCCESS';
export const GET_STATS_RANDOM_PROJECTS_FAILURE = 'stats/STATS_RANDOM_PROJECTS_FAILURE';
export const GET_STATS_RANDOM_PROJECTS_END =     'stats/STATS_RANDOM_PROJECTS_END';

export const GET_STATS_USERS_MOST_STARS_REQUEST = 'stats/STATS_USERS_MOST_STARS_REQUEST';
export const GET_STATS_USERS_MOST_STARS_SUCCESS = 'stats/STATS_USERS_MOST_STARS_SUCCESS';
export const GET_STATS_USERS_MOST_STARS_FAILURE = 'stats/STATS_USERS_MOST_STARS_FAILURE';
export const GET_STATS_USERS_MOST_STARS_END =     'stats/STATS_USERS_MOST_STARS_END';

export const GET_STATS_ORGANISATION_LANGUAGE_TRENDS_REQUEST = 'stats/STATS_ORGANISATION_LANGUAGE_TRENDS_REQUEST';
export const GET_STATS_ORGANISATION_LANGUAGE_TRENDS_SUCCESS = 'stats/STATS_ORGANISATION_LANGUAGE_TRENDS_SUCCESS';
export const GET_STATS_ORGANISATION_LANGUAGE_TRENDS_FAILURE = 'stats/STATS_ORGANISATION_LANGUAGE_TRENDS_FAILURE';
export const GET_STATS_ORGANISATION_LANGUAGE_TRENDS_END =     'stats/STATS_ORGANISATION_LANGUAGE_TRENDS_END';


// Reducer
const INITIAL_STATE = {
  statsOrganisation: {
    loading: false,
    fetched: false,
    organisationRepositories: {
      progress: []
    },
    organisationCommits: {
      progress: []
    },
    organisationUsers: {
      progress: []
    },
    organisationStars: {
      progress: []
    }
  },
  latestProjects: {
    loading: false,
    fetched: false,
    users: [],
    organisation: []
  },
  randomProjects: {
    loading: false,
    fetched: false,
    list: []
  },
  usersMostStars: {
    loading: false,
    fetched: false,
    list: []
  },
  organisationLanguageTrends: {
    loading: false,
    fetched: false,
    languages: {}
  }
}


export function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case GET_STATS_ORGANISATION_REQUEST: {
      return {
        ...state,
        statsOrganisation: {
          ...state.statsOrganisation,
          loading: true
        }
      }
    }
    case GET_STATS_ORGANISATION_SUCCESS: {
      return {
        ...state,
        statsOrganisation: {
          ...state.statsOrganisation,
          ...action.result,
          fetched: true
        }
      }
    }
    case GET_STATS_ORGANISATION_END: {
      return {
        ...state,
        statsOrganisation: {
          ...state.statsOrganisation,
          loading: false
        }
      }
    }
    case GET_STATS_LATEST_PROJECTS_REQUEST: {
      return {
        ...state,
        latestProjects: {
          ...state.latestProjects,
          loading: true
        }
      }
    }
    case GET_STATS_LATEST_PROJECTS_SUCCESS: {
      return {
        ...state,
        latestProjects: {
          ...state.latestProjects,
          ...action.result,
          fetched: true
        }
      }
    }
    case GET_STATS_LATEST_PROJECTS_END: {
      return {
        ...state,
        latestProjects: {
          ...state.latestProjects,
          loading: false
        }
      }
    }
    case GET_STATS_RANDOM_PROJECTS_REQUEST: {
      return {
        ...state,
        randomProjects: {
          ...state.randomProjects,
          loading: true
        }
      }
    }
    case GET_STATS_RANDOM_PROJECTS_SUCCESS: {
      return {
        ...state,
        randomProjects: {
          ...state.randomProjects,
          list: action.result,
          fetched: true
        }
      }
    }
    case GET_STATS_RANDOM_PROJECTS_END: {
      return {
        ...state,
        randomProjects: {
          ...state.randomProjects,
          loading: false
        }
      }
    }
    case GET_STATS_USERS_MOST_STARS_REQUEST: {
      return {
        ...state,
        usersMostStars: {
          ...state.usersMostStars,
          loading: true
        }
      }
    }
    case GET_STATS_USERS_MOST_STARS_SUCCESS: {
      return {
        ...state,
        usersMostStars: {
          ...state.usersMostStars,
          list: action.result,
          fetched: true
        }
      }
    }
    case GET_STATS_USERS_MOST_STARS_END: {
      return {
        ...state,
        usersMostStars: {
          ...state.usersMostStars,
          loading: false
        }
      }
    }
    case GET_STATS_ORGANISATION_LANGUAGE_TRENDS_REQUEST: {
      return {
        ...state,
        organisationLanguageTrends: {
          ...state.organisationLanguageTrends,
          loading: true
        }
      }
    }
    case GET_STATS_ORGANISATION_LANGUAGE_TRENDS_SUCCESS: {
      return {
        ...state,
        organisationLanguageTrends: {
          ...state.organisationLanguageTrends,
          ...action.result,
          fetched: true
        }
      }
    }
    case GET_STATS_ORGANISATION_LANGUAGE_TRENDS_END: {
      return {
        ...state,
        organisationLanguageTrends: {
          ...state.organisationLanguageTrends,
          loading: false
        }
      }
    }
    default:
      return state
  }
}


export function getStatsOrganisationRequest() {
  return {
    types: [
      GET_STATS_ORGANISATION_REQUEST,
      GET_STATS_ORGANISATION_SUCCESS,
      GET_STATS_ORGANISATION_FAILURE,
      GET_STATS_ORGANISATION_END
    ],
    promise: (getState) => ajax(
      getStatsOrganisation(getState().authentication.token)
    ).pipe(
      map((res) => res.response),
      catchError((error) => Promise.reject(error)),
    ).toPromise()
  }
}

export function getStatsLatestProjectsRequest() {
  return {
    types: [
      GET_STATS_LATEST_PROJECTS_REQUEST,
      GET_STATS_LATEST_PROJECTS_SUCCESS,
      GET_STATS_LATEST_PROJECTS_FAILURE,
      GET_STATS_LATEST_PROJECTS_END
    ],
    promise: (getState) => ajax(
      getStatsLatestProjects(getState().authentication.token)
    ).pipe(
      map((res) => res.response),
      catchError((error) => Promise.reject(error)),
    ).toPromise()
  }
}

export function getStatsRandomProjectsRequest() {
  return {
    types: [
      GET_STATS_RANDOM_PROJECTS_REQUEST,
      GET_STATS_RANDOM_PROJECTS_SUCCESS,
      GET_STATS_RANDOM_PROJECTS_FAILURE,
      GET_STATS_RANDOM_PROJECTS_END
    ],
    promise: (getState) => ajax(
      getStatsRandomProjects(getState().authentication.token)
    ).pipe(
      map((res) => res.response),
      catchError((error) => Promise.reject(error)),
    ).toPromise()
  }
}

export function getUsersMostStarsRequest() {
  return {
    types: [
      GET_STATS_USERS_MOST_STARS_REQUEST,
      GET_STATS_USERS_MOST_STARS_SUCCESS,
      GET_STATS_USERS_MOST_STARS_FAILURE,
      GET_STATS_USERS_MOST_STARS_END
    ],
    promise: (getState) => ajax(
      getStatsUsersMostStars(getState().authentication.token)
    ).pipe(
      map((res) => res.response),
      catchError((error) => Promise.reject(error)),
    ).toPromise()
  }
}


export function getOrganisationLanguageTrendsRequest() {
  return {
    types: [
      GET_STATS_ORGANISATION_LANGUAGE_TRENDS_REQUEST,
      GET_STATS_ORGANISATION_LANGUAGE_TRENDS_SUCCESS,
      GET_STATS_ORGANISATION_LANGUAGE_TRENDS_FAILURE,
      GET_STATS_ORGANISATION_LANGUAGE_TRENDS_END
    ],
    promise: (getState) => ajax(
      getOrganisationLanguageTrends(getState().authentication.token)
    ).pipe(
      map((res) => res.response),
      catchError((error) => Promise.reject(error)),
    ).toPromise()
  }
}
