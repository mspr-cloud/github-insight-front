import { ajax } from 'rxjs/ajax'
import { empty } from 'rxjs';
import {map, expand, toArray, merge, catchError} from 'rxjs/operators';
import { getRepositories, getCommitsRepositories } from 'store/services/repositories'
import _ from 'lodash';

export const GET_REPOSITORIES_REQUEST = 'repositories/REPOSITORIES_REQUEST';
export const GET_REPOSITORIES_SUCCESS = 'repositories/REPOSITORIES_SUCCESS';
export const GET_REPOSITORIES_FAILURE = 'repositories/REPOSITORIES_FAILURE';
export const GET_REPOSITORIES_END = 'repositories/REPOSITORIES_END';

export const GET_COMMITS_REPOSITORIES_REQUEST = 'repositories/COMMITS_REPOSITORIES_REQUEST';
export const GET_COMMITS_REPOSITORIES_SUCCESS = 'repositories/COMMITS_REPOSITORIES_SUCCESS';
export const GET_COMMITS_REPOSITORIES_FAILURE = 'repositories/COMMITS_REPOSITORIES_FAILURE';
export const GET_COMMITS_REPOSITORIES_END = 'repositories/COMMITS_REPOSITORIES_END';


// Reducer
const INITIAL_STATE = {
  fetched: false,
  loading: false,
  list: [],
  languages: [],
  loadingCommits: false,
}


export function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case GET_REPOSITORIES_REQUEST: {
      return {
        ...state,
        loading: true
      }
    }
    case GET_REPOSITORIES_SUCCESS: {
      let languages = [... new Set(action.result.filter((res) => res.primaryLanguage)
        .map((res) => res.primaryLanguage.name))]

      return {
        ...state,
        fetched: true,
        list: action.result,
        languages: languages.map((langague) => ({
          ..._.find(action.result, {primaryLanguage: {name: langague}}).primaryLanguage,
          count: _.sumBy(action.result.filter((res) => res.primaryLanguage), ({primaryLanguage}) => primaryLanguage.name === langague)
        })).sort((a, b) => b.count - a.count)
      }
    }
    case GET_REPOSITORIES_END: {
      return {
        ...state,
        loading: false
      }
    }
    case GET_COMMITS_REPOSITORIES_REQUEST: {
      return {
        ...state,
        loadingCommits: true
      }
    }
    case GET_COMMITS_REPOSITORIES_SUCCESS: {
      return {
        ...state,
        list: state.list.map((repository) => {
          if(repository.name === action.result.name) {
            repository.commits = action.result.list;
          }
          return repository
        })
      }
    }
    case GET_COMMITS_REPOSITORIES_END: {
      return {
        ...state,
        loadingCommits: false
      }
    }
    default:
      return state
  }
}

const makeRepositoryAjax = (token, page) => ajax(
  getRepositories(token, page)
).pipe(
  map((res) => res.response)
)

export function getRepositoriesRequest() {
  let page = 0;
  return {
    types: [
      GET_REPOSITORIES_REQUEST,
      GET_REPOSITORIES_SUCCESS,
      GET_REPOSITORIES_FAILURE,
      GET_REPOSITORIES_END
    ],
    promise: (getState) => makeRepositoryAjax(
      getState().authentication.token,
      page
    ).pipe(
      expand((data) => {
        if (data.hasNextPage) {
          page++;
          return makeRepositoryAjax(
            getState().authentication.token,
            page
          )
        }
        return empty();
      }),
      map((data) => data.repositories),
      toArray(),
      map((data) => data.flat())
    ).toPromise()
  }
}


const makeCommitRepositoryAjax = (token, page, repository, start, end) => ajax(
  getCommitsRepositories(token, page, repository, start, end)
).pipe(
  map((res) => res.response)
)

export function getCommitsRepositoriesRequest(repository, start, end) {
  let page = 0;

  return {
    types: [
      GET_COMMITS_REPOSITORIES_REQUEST,
      GET_COMMITS_REPOSITORIES_SUCCESS,
      GET_COMMITS_REPOSITORIES_FAILURE,
      GET_COMMITS_REPOSITORIES_END
    ],
    promise: (getState) => makeCommitRepositoryAjax(
      getState().authentication.token,
      page,
      repository,
      start,
      end
    ).pipe(
      expand((data) => {
        if (data.hasNextPage) {
          page++;
          return makeCommitRepositoryAjax(
            getState().authentication.token,
            page,
            repository,
            start,
            end
          )
        }
        return empty();
      }),
      map((data) => data.commits),
      toArray(),
      map((data) => ({list: data.flat(), name: repository}))
    ).toPromise()
  }
}
