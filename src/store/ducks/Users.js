import { ajax } from 'rxjs/ajax'
import {map, catchError} from 'rxjs/operators';
import { getUsers, updateUserRole, deleteUser } from 'store/services/users'

export const GET_USERS_REQUEST = 'users/GET_USERS_REQUEST';
export const GET_USERS_SUCCESS = 'users/GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'users/GET_USERS_FAILURE';
export const GET_USERS_END = 'users/GET_USERS_END';

export const UPDATE_USER_ROLE_REQUEST = 'users/UPDATE_USER_ROLE_REQUEST';
export const UPDATE_USER_ROLE_SUCCESS = 'users/UPDATE_USER_ROLE_SUCCESS';
export const UPDATE_USER_ROLE_FAILURE = 'users/UPDATE_USER_ROLE_FAILURE';
export const UPDATE_USER_ROLE_END = 'users/UPDATE_USER_ROLE_END';

export const DELETE_USER_REQUEST = 'users/DELETE_USER_REQUEST';
export const DELETE_USER_SUCCESS = 'users/DELETE_USER_SUCCESS';
export const DELETE_USER_FAILURE = 'users/DELETE_USER_FAILURE';
export const DELETE_USER_END =     'users/DELETE_USER_END';


// Reducer
const INITIAL_STATE = {
    list: [],
    fetched: false,
    loading: false,
    loadingTable: false,
}


export function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case DELETE_USER_REQUEST:
        case UPDATE_USER_ROLE_REQUEST: {
            return {
                ...state,
              loadingTable: true,
            }
        }
        case GET_USERS_REQUEST: {
            return {
                ...state,
                loading: true,
            }
        }
        case GET_USERS_SUCCESS: {
            return {
                ...state,
                fetched: true,
                list: action.result,
            }
        }
        case DELETE_USER_END:
        case UPDATE_USER_ROLE_END: {
            return {
                ...state,
              loadingTable: false,
            }
        }
        case GET_USERS_END: {
            return {
                ...state,
                loading: false,
            }
        }
        case UPDATE_USER_ROLE_SUCCESS: {
            return {
                ...state,
                list: state.list.map((user) => {
                    if(user.id === action.result.id) {
                        return action.result;
                    }
                    return user;
                })
            }
        }
        case DELETE_USER_SUCCESS: {
            return {
                ...state,
                list: state.list.filter((user) => user.id !== action.result.userId)
            }
        }
        default:
            return state
    }
}


export function getUsersRequest() {
    return {
        types: [
            GET_USERS_REQUEST,
            GET_USERS_SUCCESS,
            GET_USERS_FAILURE,
            GET_USERS_END
        ],
        promise: (getState) => ajax(
            getUsers(getState().authentication.token)
        ).pipe(
            map((res) => res.response),
            catchError((error) => Promise.reject(error)),
        ).toPromise()
    }
}

export function updateRoleRequest (id, role) {
    return {
        types: [
            UPDATE_USER_ROLE_REQUEST,
            UPDATE_USER_ROLE_SUCCESS,
            UPDATE_USER_ROLE_FAILURE,
            UPDATE_USER_ROLE_END
        ],
        promise: (getState) => ajax(
            updateUserRole(getState().authentication.token, ({id, role}))
        ).pipe(
            map((res) => res.response),
            catchError((error) => Promise.reject(error)),
        ).toPromise()
    }
}

export function deleteUserRequest (id) {
    return {
        types: [
            DELETE_USER_REQUEST,
            DELETE_USER_SUCCESS,
            DELETE_USER_FAILURE,
            DELETE_USER_END
        ],
        promise: (getState) => ajax(
            deleteUser(getState().authentication.token, ({id}))
        ).pipe(
            map((res) => ({...res.response, userId: id})),
            catchError((error) => Promise.reject(error)),
        ).toPromise()
    }
}
