import { ajax } from 'rxjs/ajax'
import {map, catchError} from 'rxjs/operators';
import { getOrganisationInformation } from 'store/services/organisation'

export const GET_ORGANISATION_INFORMATION_REQUEST = 'organisation/ORGANISATION_INFORMATION_REQUEST';
export const GET_ORGANISATION_INFORMATION_SUCCESS = 'organisation/ORGANISATION_INFORMATION_SUCCESS';
export const GET_ORGANISATION_INFORMATION_FAILURE = 'organisation/ORGANISATION_INFORMATION_FAILURE';
export const GET_ORGANISATION_INFORMATION_END = 'organisation/GET_ORGANISATION_INFORMATION_END';


// Reducer
const INITIAL_STATE = {
    id: null,
    avatar: null,
    name: null,
    description: null,
    fetched: false,
    loading: false
}


export function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case GET_ORGANISATION_INFORMATION_REQUEST: {
          return {
            ...state,
            loading: true
          }
        }
        case GET_ORGANISATION_INFORMATION_SUCCESS: {
            return {
                ...state,
                id: action.result._id,
                avatar: action.result.avatarUrl,
                name: action.result.name,
                description: action.result.description,
                fetched: true
            }
        }
        case GET_ORGANISATION_INFORMATION_END: {
          return {
            ...state,
            loading: false
          }
        }
        default:
            return state
    }
}


export function getOrganisationInformationRequest() {
    return {
        types: [
            GET_ORGANISATION_INFORMATION_REQUEST,
            GET_ORGANISATION_INFORMATION_SUCCESS,
            GET_ORGANISATION_INFORMATION_FAILURE,
            GET_ORGANISATION_INFORMATION_END
        ],
        promise: (getState) => ajax(
            getOrganisationInformation(getState().authentication.token)
        ).pipe(
            map((res) => res.response),
            catchError((error) => Promise.reject(error)),
        ).toPromise()
    }
}
