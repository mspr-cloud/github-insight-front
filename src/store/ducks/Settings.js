import {
  LAYOUT_TYPE,
  NAV_STYLE,
  NAV_STYLE_FIXED,
} from "../../constants/ThemeSetting";

export const LOCALSTORAGE_LOCALE = "@localstorage/locale";
export const TOGGLE_COLLAPSED_NAV = 'TOGGLE_COLLAPSE_MENU';
export const WINDOW_WIDTH = 'WINDOW-WIDTH';
export const SWITCH_LANGUAGE = 'SWITCH-LANGUAGE';
export const INIT_URL = 'INIT_URL';

const initialSettings = {
  navCollapsed: true,
  navStyle: NAV_STYLE_FIXED,
  pathname: '',
  initURL: '',
  width: window.innerWidth,
  locale: JSON.parse(localStorage.getItem(LOCALSTORAGE_LOCALE)) || {
    languageId: 'english',
    locale: 'en',
    name: 'English',
    icon: 'us'
  }
};

export function reducer(state = initialSettings, action = {}) {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
      return {
        ...state,
        pathname: action.payload.pathname,
        navCollapsed: false
      };
    case TOGGLE_COLLAPSED_NAV:
      return {
        ...state,
        navCollapsed: action.navCollapsed
      };
    case INIT_URL: {
      return {
        ...state,
        initURL: action.payload || ''
      }
    }
    case WINDOW_WIDTH:
      return {
        ...state,
        width: action.width,
      };
    case NAV_STYLE:
      return {
        ...state,
        navStyle: action.navStyle
      };
    case LAYOUT_TYPE:
      return {
        ...state,
        layoutType: action.layoutType
      };
    case SWITCH_LANGUAGE:
      return {
        ...state,
        locale: action.payload,

      };
    default:
      return state;
  }
};

export function toggleCollapsedSideNav(navCollapsed) {
  return {type: TOGGLE_COLLAPSED_NAV, navCollapsed};
}

export function updateWindowWidth(width) {
  return {type: WINDOW_WIDTH, width};
}
export function onNavStyleChange(navStyle) {
  return {type: NAV_STYLE, navStyle};
}

export function onLayoutTypeChange(layoutType) {
  return {type: LAYOUT_TYPE, layoutType};
}

export const setInitUrl = (url) => {
  return {
    type: INIT_URL,
    payload: url
  };
};

export function switchLanguage(locale) {
  localStorage.setItem(LOCALSTORAGE_LOCALE, JSON.stringify(locale))
  return {
    type: SWITCH_LANGUAGE,
    payload: locale
  };
}

