

export function getRepositories(token, page) {

    return {
        method: 'GET',
        url: `/api/repository/organisation/list?page=${page || 0}`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        }
    }
}

export function getCommitsRepositories(token, page, repository, start, end) {
    console.log(start, end)
    return {
        method: 'GET',
        url: `/api/commits/organisation/${repository}/commits?page=${page || 0}&start=${start}&end=${end}`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        }
    }
}
