
export function login(payload) {
    let data = {
        email: payload.email,
        password: payload.password
    }

    return {
        method: 'POST',
        url: `/api/login`,
        headers: {
            'Content-Type': "application/json"
        },
        body: data,
    }
}


export function register(payload) {
    let data = {
        firstName: payload.firstName,
        lastName: payload.lastName,
        email: payload.email,
        password: payload.password
    }

    return {
        method: 'POST',
        url: `/api/signup`,
        headers: {
            'Content-Type': "application/json"
        },
        body: data,
    }
}


export function getAccount(token) {
    return {
        method: 'GET',
        url: `/api/user/me`,
        headers: {
            'Content-Type': "application/json",
            'Authorization': `JWT ${token}`
        }
    }
}

export function loginCheck(refreshToken) {
    let data = {
        refreshToken: refreshToken
    }

    return {
        method: 'POST',
        url: `/api/refresh`,
        headers: {
            'Content-Type': "application/json"
        },
        body: data
    }
}
