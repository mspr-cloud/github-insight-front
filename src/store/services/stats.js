

export function getStatsOrganisation(token) {
  return {
    method: 'GET',
    url: `/api/stats/organisation`,
    headers: {
      'Content-Type': "application/json",
      Authorization: `JWT ${token}`
    }
  }
}

export function getStatsLatestProjects(token) {
  return {
    method: 'GET',
    url: `/api/stats/projects/latest`,
    headers: {
      'Content-Type': "application/json",
      Authorization: `JWT ${token}`
    }
  }
}

export function getStatsRandomProjects(token) {
  return {
    method: 'GET',
    url: `/api/stats/projects/random`,
    headers: {
      'Content-Type': "application/json",
      Authorization: `JWT ${token}`
    }
  }
}

export function getStatsUsersMostStars(token) {
  return {
    method: 'GET',
    url: `/api/stats/users/mostStars`,
    headers: {
      'Content-Type': "application/json",
      Authorization: `JWT ${token}`
    }
  }
}

export function getOrganisationLanguageTrends(token) {
  return {
    method: 'GET',
    url: `/api/stats/organisation/language/trends`,
    headers: {
      'Content-Type': "application/json",
      Authorization: `JWT ${token}`
    }
  }
}
