

export function getOrganisationInformation(token) {

    return {
        method: 'GET',
        url: `/api/orga/info`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        }
    }
}
