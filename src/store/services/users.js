

export function getUsers(token) {
    return {
        method: 'GET',
        url: `/api/user/list`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        }
    }
}

export function updateUserRole(token, payload) {

    let data = {
        role: payload.role
    }

    return {
        method: 'POST',
        url: `/api/user/${payload.id}/role`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        },
        body: data
    }
}

export function deleteUser(token, payload) {
    return {
        method: 'POST',
        url: `/api/user/${payload.id}/delete`,
        headers: {
            'Content-Type': "application/json",
            Authorization: `JWT ${token}`
        }
    }
}
