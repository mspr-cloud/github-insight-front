import React from "react";
import {FormattedMessage, injectIntl, IntlProvider} from "react-intl";
import { LOCALSTORAGE_LOCALE } from 'store/ducks/Settings';
import AppLocale from "lngProvider";
import moment from 'moment';

require('moment/locale/en-gb.js');
require('moment/locale/fr.js');

const InjectMassage = props => <FormattedMessage {...props} />;
export default injectIntl(InjectMassage, {
  withRef: false
});


let locale = 'en';
if (localStorage.getItem(LOCALSTORAGE_LOCALE)) {
  locale = JSON.parse(localStorage.getItem(LOCALSTORAGE_LOCALE)).locale
}

moment.locale(locale);

export const intl = new IntlProvider({
  locale: locale,
  messages: AppLocale[locale].messages
}, {}).getChildContext().intl;

