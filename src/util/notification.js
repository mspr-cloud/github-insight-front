import { notification } from 'antd';

export const openNotificationByType = (type, title, message) => {
  notification[type]({
    message: title,
    description: message,
    duration: 2
  });
};
