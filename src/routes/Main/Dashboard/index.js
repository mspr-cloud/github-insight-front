import React, {Component} from 'react'
import Auxiliary from "util/Auxiliary";
import {Col, Row} from "antd";
import {Area, YAxis, AreaChart, ResponsiveContainer, Tooltip} from "recharts";
import Chart from "components/Dashboard/Chart";
import LastProjects from "components/Dashboard/LastProjects";
import RandomProjectStep from "components/Dashboard/RandomProjectStep";
import UsersMostStars from "components/Dashboard/UsersMostStars";
import LanguageTrends from "components/Dashboard/LanguageTrends";
import {connect} from "react-redux";
import {injectIntl} from "react-intl";
import {
  getStatsOrganisationRequest,
  getStatsLatestProjectsRequest,
  getStatsRandomProjectsRequest,
  getUsersMostStarsRequest,
  getOrganisationLanguageTrendsRequest
} from 'store/ducks/Stats'
import _ from 'lodash';
import moment from 'moment';
import IntlMessages, { intl } from "util/IntlMessages";

class Dashboard extends Component {

  formatChartData = (datas) => {
    let result = [];
    _.forIn(datas,(value, key) => {
      result.push({name: moment(key).format('L'), value, unix: moment(key).unix()})
    });

    result.sort((a, b) => a.unix - b.unix);

    return result;
  }

  calcPercentage = (datas) => {
    let percent;
    datas = this.formatChartData(datas);

    if (datas.length === 0) {
      percent = 0;
    } else {
      let last = datas[0];
      let first = datas[datas.length-1];
      percent = Math.round((first.value - last.value) / last.value * 100);
    }

    return `${percent}%`
  }

  componentWillMount() {
    const { stats } = this.props;

    if(!stats.statsOrganisation.fetched && !stats.statsOrganisation.loading) {
      this.props.statsOrganisation();
    }

    if(!stats.latestProjects.fetched && !stats.latestProjects.loading) {
      this.props.statsLatestProjects();
    }

    if(!stats.randomProjects.fetched && !stats.randomProjects.loading) {
      this.props.statsRandomProjects();
    }

    if(!stats.usersMostStars.fetched && !stats.usersMostStars.loading) {
      this.props.statsUsersMostStars();
    }

    if(!stats.organisationLanguageTrends.fetched && !stats.organisationLanguageTrends.loading) {
      this.props.statsOrganisationLanguageTrends();
    }
  }

  render() {

    const { stats: {
      statsOrganisation,
      latestProjects,
      randomProjects,
      usersMostStars,
      organisationLanguageTrends
    } } = this.props;

    const {
      organisationRepositories,
      organisationCommits,
      organisationUsers,
      organisationStars
    } = statsOrganisation;

    const loading = (statsOrganisation.loading && !statsOrganisation.fetched);

    return (
      <Auxiliary>
        <Row>
          <Col xl={6} lg={12} md={12} sm={12} xs={24}>
            <Chart chartProperties={{
              title: <IntlMessages id="app.dashboard.chart.repositories.title"/>,
              prize: organisationRepositories && (organisationRepositories.total),
              icon: 'stats',
              bgColor: 'primary',
              styleName: 'up',
              desc: <IntlMessages id="app.dashboard.chart.week"/>,
              percent: organisationRepositories && (this.calcPercentage(organisationRepositories.progress)),
              loading
            }}
            children={(!statsOrganisation.loading && statsOrganisation.fetched) ?
              <ResponsiveContainer width="100%" height={75}>
                <AreaChart data={this.formatChartData(organisationRepositories.progress)}
                           margin={{top: 0, right: 0, left: 0, bottom: 0}}>
                  <YAxis domain={['dataMin - 2', 'dataMax + 2']} hide />
                  <Tooltip labelFormatter={(index) => this.formatChartData(organisationRepositories.progress)[index].name} />
                  <Area dataKey="value" strokeWidth={1} stroke='#092453' fill="#092453"
                        fillOpacity={1}/>
                </AreaChart>
              </ResponsiveContainer> : null
            }/>
          </Col>
          <Col xl={6} lg={12} md={12} sm={12} xs={24}>
            <Chart
              chartProperties={{
                title: <IntlMessages id="app.dashboard.chart.commits.title"/>,
                prize: organisationCommits && (organisationCommits.total),
                icon: 'stats',
                bgColor: 'orange',
                styleName: 'up',
                desc: <IntlMessages id="app.dashboard.chart.week"/>,
                percent: organisationCommits && (this.calcPercentage(organisationCommits.progress)),
                loading
              }}
               children={(!statsOrganisation.loading && statsOrganisation.fetched) ?
                 <ResponsiveContainer width="100%" height={75}>
                   <AreaChart data={this.formatChartData(organisationCommits.progress)}
                              margin={{top: 0, right: 0, left: 0, bottom: 0}}>
                     <YAxis domain={['dataMin - 2', 'dataMax + 2']} hide />
                     <Tooltip labelFormatter={(index) => this.formatChartData(organisationCommits.progress)[index].name} />
                     <Area dataKey="value" strokeWidth={1} stroke='#092453' fill="#092453"
                           fillOpacity={1}/>
                   </AreaChart>
                 </ResponsiveContainer> : null
             }/>
          </Col>
          <Col xl={6} lg={12} md={12} sm={12} xs={24}>
            <Chart
              chartProperties={{
                title: <IntlMessages id="app.dashboard.chart.users.title"/>,
                prize: organisationUsers && (organisationUsers.total),
                icon: 'stats',
                bgColor: 'teal',
                styleName: 'down',
                desc: <IntlMessages id="app.dashboard.chart.week"/>,
                percent: organisationUsers && (this.calcPercentage(organisationUsers.progress)),
                loading
              }}
               children={(!statsOrganisation.loading && statsOrganisation.fetched) ?
                 <ResponsiveContainer width="100%" height={75}>
                   <AreaChart data={this.formatChartData(organisationUsers.progress)}
                              margin={{top: 0, right: 0, left: 0, bottom: 0}}>
                     <YAxis domain={['dataMin - 2', 'dataMax + 2']} hide />
                     <Tooltip labelFormatter={(index) => this.formatChartData(organisationUsers.progress)[index].name} />
                     <Area dataKey="value" strokeWidth={1} stroke='#092453' fill="#092453"
                           fillOpacity={1}/>
                   </AreaChart>
                 </ResponsiveContainer> : null
             }/>
          </Col>
          <Col xl={6} lg={12} md={12} sm={12} xs={24}>
            <Chart
              chartProperties={{
                title: <IntlMessages id="app.dashboard.chart.stars.title"/>,
                prize: organisationStars && (organisationStars.total),
                icon: 'stats',
                bgColor: 'pink',
                styleName: 'down',
                desc: <IntlMessages id="app.dashboard.chart.week"/>,
                percent: organisationStars && (this.calcPercentage(organisationStars.progress)),
                loading
              }}
               children={(!statsOrganisation.loading && statsOrganisation.fetched) ?
                 <ResponsiveContainer width="100%" height={75}>
                   <AreaChart data={this.formatChartData(organisationStars.progress)}
                              margin={{top: 0, right: 0, left: 0, bottom: 0}}>
                     <YAxis domain={['dataMin - 2', 'dataMax + 2']} hide />
                     <Tooltip labelFormatter={(index) => this.formatChartData(organisationStars.progress)[index].name} />
                     <Area dataKey="value" strokeWidth={1} stroke='#092453' fill="#092453"
                           fillOpacity={1}/>
                   </AreaChart>
                 </ResponsiveContainer> : null
             }/>
          </Col>
          <Col xl={12} lg={24} md={12} sm={24} xs={24}>
            <UsersMostStars usersMostStars={usersMostStars}/>
          </Col>
          <Col xl={12} lg={24} md={12} sm={24} xs={24}>
            <LastProjects latestProjects={latestProjects} />
          </Col>
          <Col xl={6} lg={12} md={6} sm={12} xs={24}>
            <LanguageTrends rank={"first"} organisationLanguageTrends={organisationLanguageTrends}/>
          </Col>
          <Col xl={6} lg={12} md={6} sm={12} xs={24}>
            <LanguageTrends rank={"second"} organisationLanguageTrends={organisationLanguageTrends}/>
          </Col>
          <Col xl={12} lg={24} md={12} sm={24} xs={24}>
            <RandomProjectStep randomProjects={randomProjects}/>
          </Col>
        </Row>
      </Auxiliary>
    );
  }
}

const mapStateToProps = state => ({
  stats: state.stats,
})

const mapDispatchToProps = dispatch => {
  return {
    statsOrganisation: () => dispatch(getStatsOrganisationRequest()),
    statsLatestProjects: () => dispatch(getStatsLatestProjectsRequest()),
    statsRandomProjects: () => dispatch(getStatsRandomProjectsRequest()),
    statsUsersMostStars: () => dispatch(getUsersMostStarsRequest()),
    statsOrganisationLanguageTrends: () => dispatch(getOrganisationLanguageTrendsRequest())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Dashboard));

