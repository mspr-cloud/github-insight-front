import {
    Table as AntTable, Input, Button, Icon, Menu, Dropdown, Popconfirm
} from 'antd';
import Highlighter from 'react-highlight-words';
import {injectIntl} from "react-intl";
import {Component} from "react";
import React from "react";
import PropTypes from 'prop-types';
import IntlMessages, { intl } from "util/IntlMessages";

const SubMenu = Menu.SubMenu;

class Table extends Component {

    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        users: PropTypes.array.isRequired,
        onUpdateRole: PropTypes.func.isRequired,
        onDeleteUser: PropTypes.func.isRequired,
        loadingTable: PropTypes.bool.isRequired
    };

    state = {
        searchText: '',
    };

    formatedData = () => this.props.users.map((user) => ({
        key: user.id,
        name: `${user.firstName} ${user.lastName}`,
        email: user.email,
        role: intl.formatMessage({ id: `app.shared.${user.role}` })
    }))

    confirm = (user) => {
        this.props.onDeleteUser(user.key)
    }



    renderMenu = (id, role) => (
        <Menu key="menu">
            <Menu.Item key="admin" onClick={() => this.props.onUpdateRole(id, 'admin')} disabled={role ===  intl.formatMessage({ id: `app.shared.admin` })}>
                <IntlMessages key="admin.action" id="app.shared.admin" />
            </Menu.Item>
            <Menu.Divider key="divider-0" />
            <Menu.Item key="member" onClick={() => this.props.onUpdateRole(id, 'member')} disabled={role ===  intl.formatMessage({ id: `app.shared.member` })}>
                <IntlMessages key="member.action" id="app.shared.member" />
            </Menu.Item>
            <Menu.Divider key="divider-1" />
            <Menu.Item key="user" onClick={() => this.props.onUpdateRole(id, 'user')} disabled={role ===  intl.formatMessage({ id: `app.shared.user` })}>
                <IntlMessages key="user.action" id="app.shared.user" />
            </Menu.Item>
        </Menu>
    );

    getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
                             setSelectedKeys, selectedKeys, confirm, clearFilters,
                         }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => { this.searchInput = node; }}
                    placeholder={`${intl.formatMessage({ id: `app.shared.search` })} ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => this.handleSearch(selectedKeys, confirm)}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    <IntlMessages id="app.shared.search" />
                </Button>
                <Button
                    onClick={() => this.handleReset(clearFilters)}
                    size="small"
                    style={{ width: 90 }}
                >
                    <IntlMessages id="app.shared.reset" />
                </Button>
            </div>
        ),
        filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
        onFilter: (value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: (visible) => {
            if (visible) {
                setTimeout(() => this.searchInput.select());
            }
        },
        render: (text) => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[this.state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    })


    handleSearch = (selectedKeys, confirm) => {
        confirm();
        this.setState({ searchText: selectedKeys[0] });
    }

    handleReset = (clearFilters) => {
        clearFilters();
        this.setState({ searchText: '' });
    }


    render() {
        const { currentUser, loadingTable } = this.props;

        const columns = [{
            title: intl.formatMessage({ id: `app.shared.name` }),
            dataIndex: 'name',
            key: 'name',
            width: '30%',
            ...this.getColumnSearchProps('name'),
        }, {
            title: intl.formatMessage({ id: `app.shared.email` }),
            dataIndex: 'email',
            key: 'email',
            width: '20%',
            ...this.getColumnSearchProps('email'),
        }, {
            title: intl.formatMessage({ id: `app.shared.role` }),
            dataIndex: 'role',
            key: 'role',
            ...this.getColumnSearchProps('role'),
            render: (text, record) => (
                <Dropdown overlay={this.renderMenu(record.key, record.role)} disabled={record.key === currentUser.id}>
                    <Button style={{ marginLeft: 8 }}>
                        {record.role} <Icon type="down" />
                    </Button>
                </Dropdown>
            )
        },{
            title: intl.formatMessage({ id: `app.shared.action` }),
            key: 'action',
            render: (text, record) =>
                    record.key === currentUser.id ?
                    <Button type="danger" disabled={true}>
                        <IntlMessages id="app.shared.delete" />
                    </Button>
                    :
                    <Popconfirm
                        title={<IntlMessages id="app.administration.users.deleteConfirm" />}
                        onConfirm={() => this.confirm(record)}
                        okText={<IntlMessages id="app.shared.yes" />}
                        cancelText={<IntlMessages id="app.shared.no" />}
                    >
                        <Button type="danger">
                            <IntlMessages id="app.shared.delete" />
                        </Button>
                    </Popconfirm>
        }];

        return (
            <AntTable
                className="gi-table-responsive"
                columns={columns}
                loading={loadingTable}
                dataSource={this.formatedData()}
                locale={{
                    filterTitle: intl.formatMessage({ id: `app.shared.filterTitle` }),
                    filterConfirm: intl.formatMessage({ id: `app.shared.filterConfirm` }),
                    filterReset: intl.formatMessage({ id: `app.shared.filterReset` }),
                    emptyText: intl.formatMessage({ id: `app.shared.emptyText` }),
                }}
            />
        )
    }
}


export default injectIntl(Table)
