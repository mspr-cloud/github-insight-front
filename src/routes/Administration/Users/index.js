import React, {Component, Fragment} from 'react'
import Table from './Table';
import {Card, Col, Row} from "antd";
import { injectIntl } from "react-intl";
import Widget from "components/Widget";
import connect from "react-redux/es/connect/connect";
import { getUsersRequest, updateRoleRequest, deleteUserRequest } from 'store/ducks/Users'
import IntlMessages, { intl } from "util/IntlMessages";
import CircularProgress from 'components/CircularProgress';

class AdministrationUsers extends Component {

  componentWillMount() {
    const { users } = this.props;

    if(!users.fetched) {
      this.props.getUsers()
    }

  }

    render() {

        const { users, currentUser, updateRole, deleteUser } = this.props;
        const { loading, loadingTable } = users;

        if (loading) {
          return <CircularProgress/>
        }

        return (
          <Row>
            <Col span={24}>
              <Widget title={<IntlMessages id="app.administration.users.title" />} styleName="gi-card-tabs gi-card-tabs-right gi-card-profile">
                <div className="gi-mt-4">
                  <Table
                    loadingTable={loadingTable}
                    currentUser={currentUser}
                    users={users.list}
                    onUpdateRole={updateRole}
                    onDeleteUser={deleteUser}
                  />
                </div>
              </Widget>
            </Col>
          </Row>
        )
    }
}


const mapStateToProps = state => ({
  users: state.users,
  currentUser: state.authentication
})

const mapDispatchToProps = dispatch => {
  return {
    updateRole: (userId, role) => dispatch(updateRoleRequest(userId, role)),
    deleteUser: (userId) => dispatch(deleteUserRequest(userId)),
    getUsers: () => dispatch(getUsersRequest())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(AdministrationUsers));
