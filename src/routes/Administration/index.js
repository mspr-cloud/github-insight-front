import React from "react";
import {Route, Switch} from "react-router-dom";
import Users from "./Users";

const Administration = ({match}) => (
  <Switch>
    <Route path={`${match.url}/users`} component={Users}/>
  </Switch>
);

export default Administration;
