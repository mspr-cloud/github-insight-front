import React from "react";
import {Route, Switch} from "react-router-dom";
import Administration from './Administration'
import Organisation from './Organisation'
import Main from './Main'
import asyncComponent from "util/asyncComponent";

const App = ({match}) => (
  <div className="gi-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}main`} component={Main}/>
      <Route path={`${match.url}administration`} component={Administration}/>
      <Route path={`${match.url}organisation`} component={Organisation}/>
    </Switch>
  </div>
);

export default App;
