import React from "react";
import {Route, Switch} from "react-router-dom";
import List from './List';
import View from './View';


const Organisation = ({match}) => (
  <Switch>
    <Route path={`${match.url}/list`} component={List}/>
    <Route path={`${match.url}/repository/:name/view`} component={View}/>
  </Switch>
);

export default Organisation;
