import React, {Component, Fragment} from "react";
import {Switch, Drawer, Icon} from "antd";

import CustomScrollbars from "util/CustomScrollbars";
import RepositoryList from "components/Repository/RepositoryList";
import AppModuleHeader from "components/AppModuleHeader/index";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";
import {connect} from "react-redux";
import {injectIntl} from "react-intl";
import { getRepositoriesRequest } from 'store/ducks/Repositories'
import { intl } from "util/IntlMessages";


class OrganisationList extends Component {

  constructor() {
    super();
    this.state = {
      drawerState: false,
      search: false,
      searchValue: '',
      filter: false,
      filterValue: '',
      isPrivate: false
    }
  }

  componentWillMount() {
    const { repositories } = this.props;

    if(!repositories.fetched && !repositories.loading) {
      this.props.getRepositories();
    }

  }

  getNavLabels = () => {
    return this.props.repositories.languages.map((language, index) =>
      <li key={index} onClick={() => this.updateFilter(language)}>
        <span className={`gi-link ${this.state.filterValue === language.name && ("active")}`}>
          <i style={{color: language.color}} className={`icon icon-circle`}/>
          <span>{language.name} ({language.count})</span>
        </span>
      </li>
    )
  };

  getNavFilters = () => {
    return (
      <li key={"private"}>
        <div className="gi-link gi-mb-0">
          <Switch className="gi-mr-3" onChange={(value) => this.setState({isPrivate: value})} checkedChildren={<Icon type="check"/>}
                  checked={this.state.isPrivate}
                  unCheckedChildren={<Icon type="cross"/>}
          />
          <IntlMessages id="app.organisation.repositories.filter.private"/>
        </div>
      </li>
    )
  }

  RepositorySideBar = () => {
    return <div className="gi-module-side">
      <div className="gi-module-side-header">
        <div className="gi-module-logo">
          <i className="icon icon-pagination gi-mr-4"/>
          <IntlMessages id="sidebar.list"/>
        </div>

      </div>
      <div className="gi-module-side-content">
        <CustomScrollbars className="gi-module-side-scroll">
          <ul className="gi-module-nav">
            <li onClick={() => this.resetFilter()}>
            <span className={`gi-link ${!this.state.filter && !this.state.isPrivate && ("active")}`}>
                <i className="icon icon-all-contacts gi-pt-1"/>
                <span><IntlMessages id="app.organisation.repositories.filter.language.all"/></span>
              </span>
            </li>
            <li className="gi-module-nav-label">
              <IntlMessages id="app.organisation.repositories.filter.language"/>
            </li>
            {this.getNavLabels()}
            <li className="gi-module-nav-label">
              <IntlMessages id="app.organisation.repositories.filter.default"/>
            </li>
            {this.getNavFilters()}
          </ul>
        </CustomScrollbars>
      </div>
    </div>
  };

  onRepositorySelect() {
    setTimeout(() => {
      this.setState({loader: false});
    }, 1500);
  }

  onToggleDrawer() {
    this.setState({
      drawerState: !this.state.drawerState
    });
  }

  updateFilter (language) {

    const { filter, filterValue } = this.state;

    if(filter && filterValue === language.name) {
      this.setState({
        filter: false,
        filterValue: ''
      })
    } else {
      this.setState({
        filter: true,
        filterValue: language.name
      })
    }
  }

  handleFilter(repositories) {
    return repositories
      .filter((repository) => repository.primaryLanguage)
      .filter((repository) => repository.primaryLanguage.name === this.state.filterValue)
  }

  resetFilter() {
    this.setState({
      filter: false,
      filterValue: '',
      isPrivate: false
    })
  }

  updateSearch(evt) {
    const searchText= evt.target.value;
    if (searchText === '') {
      this.setState({
        search: false,
        searchValue: searchText,
        query: searchText.toLowerCase()
      });
    } else {
      this.setState({
        search: true,
        searchValue: searchText,
        query: searchText.toLowerCase(),
      });
    }
  }

  handleSearch (repositories)  {
    return repositories.filter((repository) =>
      repository.name.toLowerCase().indexOf(this.state.query) > -1);
  }

  handlePrivate (repositories) {
    return repositories.filter((repository) => repository.isPrivate)
  }

  render() {
    const { drawerState, searchValue, search, filter, isPrivate } = this.state;
    const { repositories } = this.props;

    let repositoriesList = repositories.list

    if (search) {
      repositoriesList = this.handleSearch(repositoriesList);
    }

    if (filter) {
      repositoriesList = this.handleFilter(repositoriesList);
    }

    if (isPrivate) {
      repositoriesList = this.handlePrivate(repositoriesList);
    }

    return (
      <div className="gi-main-content">
        <div className="gi-app-module">
          {
            repositories.loading ?
              <div className="gi-loader-view">
                <CircularProgress/>
              </div>
              :
              <Fragment>
                <div className="gi-d-block gi-d-lg-none">
                  <Drawer
                    placement="left"
                    closable={false}
                    visible={drawerState}
                    onClose={this.onToggleDrawer.bind(this)}>
                    {this.RepositorySideBar()}
                  </Drawer>
                </div>
                <div className="gi-module-sidenav gi-d-none gi-d-lg-flex">
                  {this.RepositorySideBar()}
                </div>

                <div className="gi-module-box">
                  <div className="gi-module-box-header">

              <span className="gi-drawer-btn gi-d-flex gi-d-lg-none">
                  <i className="icon icon-menu gi-icon-btn" aria-label="Menu"
                     onClick={this.onToggleDrawer.bind(this)}/>
              </span>
                    <AppModuleHeader placeholder={intl.formatMessage({ id: "app.organisation.repositories.search.placeholder" })}
                                     onChange={this.updateSearch.bind(this)}
                                     value={searchValue}/>
                  </div>
                  <div className="gi-module-box-content">
                    <RepositoryList repositories={repositoriesList} onSortEnd={this.onSortEnd}
                              onRepositorySelect={this.onRepositorySelect.bind(this)}
                              useDragHandle={true}/>
                  </div>
                </div>
              </Fragment>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  repositories: state.repositories,
})

const mapDispatchToProps = dispatch => {
  return {
    getRepositories: () => dispatch(getRepositoriesRequest()),
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(OrganisationList));
