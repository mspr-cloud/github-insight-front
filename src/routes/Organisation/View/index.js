import React, {Component} from "react";
import {Col, Row, Spin} from "antd";
import {connect} from "react-redux";
import {injectIntl} from "react-intl";
import { withRouter } from "react-router";

import { getCommitsRepositoriesRequest } from 'store/ducks/Repositories'
import Auxiliary from "util/Auxiliary";
import ProjectHeader from "components/Project/ProjectHeader/index";
import About from "components/Project/About/index";
import Links from "components/Project/Links/index";
import GraphCommits from "components/Project/GraphCommits/index";
import Contributors from "components/Project/Contributors/index";
import moment from 'moment';

class View extends Component {

  constructor(props) {
    super(props)

    this.state = {
      start: moment(),
      end: moment()
    }
  }

  componentWillMount() {
    if(!this.props.repository.commits) {
      this.props.getCommitRepository(
        this.props.repository.name,
        this.state.start.startOf('year').format('YYYY-MM-DD'),
        this.state.start.endOf('year').format('YYYY-MM-DD'),
      );
    }
  }

  onYearChange = (start) => {
    this.setState({
      end: moment(start),
      start: moment(start)
    }, () => {
      this.props.getCommitRepository(
        this.props.repository.name,
        moment(this.state.start).startOf('year').format('YYYY-MM-DD'),
        moment(this.state.start).endOf('year').format('YYYY-MM-DD'),
      );
    })
  }

  render() {
    const { start, end } = this.state;
    const { repository, loading } = this.props;

    return (
      <Auxiliary>
        <ProjectHeader repository={repository}/>
        <div className="gi-profile-content">
          <Row>
            <Col xl={16} lg={14} md={14} sm={24} xs={24}>
              <About repository={repository}/>
              <Spin spinning={loading}>
                <GraphCommits
                  commits={repository.commits || []}
                  start={start}
                  end={end}
                  onYearChange={this.onYearChange}
                />
              </Spin>
              <Contributors repository={repository}/>
            </Col>

            <Col xl={8} lg={10} md={10} sm={24} xs={24}>
              <Links repository={repository}/>
            </Col>
          </Row>
        </div>
      </Auxiliary>
    );
  }
}


const mapStateToProps = (state, ownProps) => {
  const repositoryName = ownProps.match.params.name
  return ({
    repository: state.repositories.list.find((repository) => repository.name === repositoryName),
    loading: state.repositories.loadingCommits,
  })
}

const mapDispatchToProps = dispatch => {
  return {
    getCommitRepository: (name, start, end) => dispatch(getCommitsRepositoriesRequest(name, start, end)),
  }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(injectIntl(View)));
