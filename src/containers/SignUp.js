import React from "react";
import {Button, Checkbox, Form, Icon, Input} from "antd";
import {Link} from "react-router-dom";
import { injectIntl } from 'react-intl'
import {connect} from "react-redux";
import {registerRequest} from 'store/ducks/Authentication';
import IntlMessages from "util/IntlMessages";
import {message} from "antd/lib/index";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignUp extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.register(values);
      }
    });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback(this.props.intl.formatMessage({ id: 'app.userAuth.signUp.error.password' }));
    } else {
      callback();
    }
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const { loading, intl} = this.props;
    return (
      <div className="gi-app-login-wrap">
        <div className="gi-app-login-container">
          <div className="gi-app-login-main-content">
            <div className="gi-app-logo-content">
              <div className="gi-app-logo-content-bg">
              </div>
              <div className="gi-app-logo-wid">
                <h1><IntlMessages id="app.userAuth.signUp"/></h1>
              </div>
            </div>

            <div className="gi-app-login-content">
              <Form onSubmit={this.handleSubmit} className="gi-signup-form gi-form-row0">
                <FormItem>
                  {getFieldDecorator('firstName', {
                    rules: [{ required: true, message: intl.formatMessage({id: 'app.userAuth.input.firstName.required'}) }],
                  })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={intl.formatMessage({ id: 'app.userAuth.signUp.firstName.label' })} />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('lastName', {
                    rules: [{ required: true, message: intl.formatMessage({id: 'app.userAuth.input.lastName.required'}) }],
                  })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={intl.formatMessage({ id: 'app.userAuth.signUp.lastName.label' })} />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('email', {
                    rules: [{ required: true, message: intl.formatMessage({id: 'app.userAuth.input.email.required'})}],
                  })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={intl.formatMessage({ id: 'app.userAuth.signUp.email.label' })} />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('password', {
                    rules: [{ required: true, message: intl.formatMessage({id: 'app.userAuth.input.password.required'}) }],
                  })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={intl.formatMessage({ id: 'app.userAuth.signUp.password.label' })} />
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('confirmPassword', {
                    rules: [{ required: true, message: intl.formatMessage({id: 'app.userAuth.input.password.required'})},
                      {
                        validator: this.compareToFirstPassword
                      }],
                  })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={intl.formatMessage({ id: 'app.userAuth.signUp.password.label' })} />
                  )}
                </FormItem>
                <FormItem>
                  <Button type="primary" className="gi-mb-0" htmlType="submit">
                    <IntlMessages id="app.userAuth.signUp"/>
                  </Button>
                  <span><IntlMessages id="app.userAuth.or"/></span> <Link to="/signin"><IntlMessages
                  id="app.userAuth.signIn"/></Link>
                </FormItem>
              </Form>
            </div>
            {loading &&
            <div className="gi-loader-view">
              <CircularProgress/>
            </div>
            }
          </div>
        </div>
      </div>

    );
  }

}

const WrappedSignUpForm = Form.create()(SignUp);

const mapStateToProps = state => ({
  loading: state.authentication.loading
});

const mapDispatchToProps = dispatch => {
  return {
    register: (values) =>  dispatch(registerRequest(values))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(WrappedSignUpForm));
