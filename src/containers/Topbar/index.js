import React, {Component} from "react";
import {Layout, Popover} from "antd";
import {Link} from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import languageData from "./languageData";
import {switchLanguage, toggleCollapsedSideNav} from "store/ducks/Settings";
import UserInfo from "components/UserInfo";
import Auxiliary from "util/Auxiliary";
import { injectIntl } from "react-intl";
import {NAV_STYLE_DRAWER, NAV_STYLE_FIXED, NAV_STYLE_MINI_SIDEBAR, TAB_SIZE} from "../../constants/ThemeSetting";
import {connect} from "react-redux";

const {Header} = Layout;

class Topbar extends Component {

  languageMenu = () => (
    <CustomScrollbars className="gi-popover-lang-scroll">
      <ul className="gi-sub-popover">
        {languageData.map(language =>
          <li className="gi-media gi-pointer" key={JSON.stringify(language)} onClick={(e) =>
            this.props.switchLanguage(language)
          }>
            <i className={`flag flag-24 gi-mr-2 flag-${language.icon}`}/>
            <span className="gi-language-text">{language.name}</span>
          </li>
        )}
      </ul>
    </CustomScrollbars>);

  render() {
    const {locale, width, navCollapsed, navStyle} = this.props;
    return (
      <Auxiliary>
        <Header>
          {navStyle === NAV_STYLE_DRAWER || ((navStyle === NAV_STYLE_FIXED || navStyle === NAV_STYLE_MINI_SIDEBAR) && width < TAB_SIZE) ?
            <div className="gi-linebar gi-mr-3">
              <i className="gi-icon-btn icon icon-menu"
                 onClick={() => {
                   this.props.toggleCollapsedSideNav(!navCollapsed);
                 }}
              />
            </div> : null}
          <Link to="/" className="gi-d-block gi-d-lg-none gi-pointer">
            <img alt="" src={require("assets/images/w-logo.png")}/></Link>
          <ul className="gi-header-notifications gi-ml-auto">
            <li className="gi-language">
              <Popover overlayClassName="gi-popover-horizantal" placement="bottomRight" content={this.languageMenu()}
                       trigger="click">
                <span className="gi-pointer gi-flex-row gi-align-items-center">
                  <i className={`flag flag-24 flag-${locale.icon}`}/>
                  <span className="gi-pl-2 gi-language-name">{locale.name}</span>
                  <i className="icon icon-chevron-down gi-pl-2"/>
                </span>
              </Popover>
            </li>
            <Auxiliary>
              <li className="gi-user-nav"><UserInfo/></li>
            </Auxiliary>
          </ul>
        </Header>
      </Auxiliary>
    );
  }
}

const mapStateToProps = ({settings}) => {
  const {locale, navStyle, navCollapsed, width} = settings;
  return {locale, navStyle, navCollapsed, width}
};

export default connect(mapStateToProps, {toggleCollapsedSideNav, switchLanguage})(injectIntl(Topbar));
