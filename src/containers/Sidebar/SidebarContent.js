import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu, Icon} from "antd";
import {Link} from "react-router-dom";
import { ROLE_ADMIN } from 'constants/Roles';
import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import OrganisationInfo from "./OrganisationInfo";

import Auxiliary from "util/Auxiliary";
import IntlMessages from "../../util/IntlMessages";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class SidebarContent extends Component {

  render() {
    const {pathname, role} = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>

        <SidebarLogo/>
        <div className="gi-sidebar-content">
          <div className={`gi-sidebar-organisation-logo`}>
            <OrganisationInfo/>
          </div>
          <CustomScrollbars className="gi-layout-sider-scrollbar">
            <Menu
              defaultOpenKeys={[defaultOpenKeys]}
              selectedKeys={[selectedKeys]}
              theme={'lite'}
              mode="inline">
              <MenuItemGroup key="main"
                             className="gi-menu-group"
                             title={<IntlMessages id="sidebar.main"/>}>
                <Menu.Item key="main/dashboard">
                  <Link to="/main/dashboard"><i className="icon icon-dasbhoard"/>
                    <IntlMessages id="sidebar.dashboard"/></Link>
                </Menu.Item>
              </MenuItemGroup>
              <MenuItemGroup key="organisation"
                             className="gi-menu-group"
                             title={<IntlMessages id="sidebar.organisation"/>}>
                <Menu.Item key="organisation/list">
                  <Link to="/organisation/list"><i className="icon icon-product-list"/>
                    <IntlMessages id="sidebar.organisation.repositories"/></Link>
                </Menu.Item>
              </MenuItemGroup>
              {
                role === ROLE_ADMIN && (
                  <MenuItemGroup key="administration"
                                 className="gi-menu-group"
                                 title={<IntlMessages id="sidebar.administration"/>}>
                    <Menu.Item key="administration/users">
                      <Link
                        to={ "/administration/users"}
                        style={{ cursor: "pointer" }}
                      >
                        <Icon type="team" />
                        <IntlMessages id="sidebar.administration.users" />
                      </Link>
                    </Menu.Item>
                  </MenuItemGroup>
                )
              }
            </Menu>
          </CustomScrollbars>
        </div>
      </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({settings, authentication}) => {
  const { locale, pathname} = settings;
  const { role } = authentication;
  return { locale, pathname, role}
};
export default connect(mapStateToProps)(SidebarContent);

