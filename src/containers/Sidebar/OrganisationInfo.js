import React, { Component, Fragment } from "react";
import { getOrganisationInformationRequest } from "store/ducks/Organisation";
import { connect } from 'react-redux'


class OrganisationInfo extends Component {

  componentWillMount() {
    const { organisation } = this.props;

    if(!organisation.fetched) {
      this.props.getOrganisation()
    }
  }

  render() {

    const { organisation: {loading, avatar, name} } = this.props

    return (
      <Fragment>
        <div className="gi-sidebar-organisation-info">
          <img src={avatar} />
          <h2>{name}</h2>
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  organisation: state.organisation
})

const mapDispatchToProps = dispatch => {
  return {
    getOrganisation: () =>  dispatch(getOrganisationInformationRequest()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrganisationInfo);
