import React from "react";
import {Button, Checkbox, Form, Icon, Input, message} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import { injectIntl } from 'react-intl'
import {loginRequest} from 'store/ducks/Authentication';
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignIn extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, {email, password}) => {
      if (!err) {
        this.props.login(email, password);
      }
    });
  };

  render() {
    const {getFieldDecorator} = this.props.form;
    const {loading, intl} = this.props;

    return (
      <div className="gi-app-login-wrap">
        <div className="gi-app-login-container">
          <div className="gi-app-login-main-content">
            <div className="gi-app-logo-content">
              <div className="gi-app-logo-content-bg">
              </div>
              <div className="gi-app-logo-wid">
                <h1><IntlMessages id="app.userAuth.signIn"/></h1>
              </div>
            </div>
            <div className="gi-app-login-content">
              <Form onSubmit={this.handleSubmit} className="gi-signin-form gi-form-row0">

                <FormItem>
                  {getFieldDecorator('email', {
                    rules: [{
                      required: true, type: 'email', message: intl.formatMessage({id: 'app.userAuth.input.email.required'}),
                    }],
                  })(
                    <Input placeholder={intl.formatMessage({id: 'app.userAuth.placeholder.email'})}/>
                  )}
                </FormItem>
                <FormItem>
                  {getFieldDecorator('password', {
                    rules: [{required: true, message: intl.formatMessage({id: 'app.userAuth.input.password.required'})}],
                  })(
                    <Input type="password" placeholder={intl.formatMessage({id: 'app.userAuth.placeholder.password'})}/>
                  )}
                </FormItem>
                <FormItem>
                  <Button type="primary" className="gi-mb-0" htmlType="submit">
                    <IntlMessages id="app.userAuth.signIn"/>
                  </Button>
                  <span><IntlMessages id="app.userAuth.or"/></span> <Link to="/signup"><IntlMessages
                  id="app.userAuth.signUp"/></Link>
                </FormItem>
              </Form>
            </div>

            {loading ?
              <div className="gi-loader-view">
                <CircularProgress/>
              </div> : null}
          </div>
        </div>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(SignIn);

const mapStateToProps = state => ({
  loading: state.authentication.loading
});


const mapDispatchToProps = dispatch => {
  return {
    login: (email, password) =>  dispatch(loginRequest({email, password}))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(WrappedNormalLoginForm));
