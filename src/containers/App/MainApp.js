import React, {Component} from "react";
import {Layout} from "antd";

import Sidebar from "../Sidebar/index";

import Topbar from "../Topbar/index";
import App from "routes/index";

const {Content, Footer} = Layout;

export class MainApp extends Component {

  render() {
    const {match} = this.props;
    return (
      <Layout className="gi-app-layout">
        <Sidebar/>
        <Layout>
          <Topbar/>
          <Content className={`gi-layout-content`}>
            <App match={match}/>
            <Footer>
              <div className="gi-layout-footer-content">
                EPSI
              </div>
            </Footer>
          </Content>
        </Layout>
      </Layout>
    )
  }
}

export default (MainApp);

