import React, {Component} from "react";
import {connect} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import {LocaleProvider} from "antd";
import {IntlProvider} from "react-intl";

import CircularProgress from 'components/CircularProgress'
import AppLocale from "lngProvider";
import MainApp from "./MainApp";
import SignIn from "../SignIn";
import SignUp from "../SignUp";
import Denied from "../Denied";
import {setInitUrl} from "store/ducks/Settings";
import {LOCALSTORAGE_REFRESH_TOKEN, loginCheckRequest} from "store/ducks/Authentication";

const RestrictedRoute = ({component: Component, isLogged, authorized, ...rest}) =>
  <Route
    {...rest}
    render={props =>
      isLogged
        ? authorized ? <Component {...props} /> : <Redirect to={'/denied'}/>
        : <Redirect
          to={{
            pathname: '/signin',
            state: {from: props.location}
          }}
        />}
  />;


class App extends Component {

  componentWillMount() {

    if (this.props.initURL === '') {
      this.props.setInitUrl(this.props.history.location.pathname);
    }
    if(localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN)){
      this.props.getRefreshToken(localStorage.getItem(LOCALSTORAGE_REFRESH_TOKEN))
    }
  }

  render() {
    const {match, location, locale, isLogged, initURL, loadingCheck, authorized} = this.props;

    if (loadingCheck) {
      return <CircularProgress className="loading"/>
    }

    if (location.pathname === '/') {
      if (!isLogged) {
        return ( <Redirect to={'/signin'}/> );
      } else if (!authorized) {
        return ( <Redirect to={'/denied'}/> );
      } else if (initURL === '' || initURL === '/' || initURL === '/signin') {
        return ( <Redirect to={'/main/dashboard'}/> );
      } else {
        return ( <Redirect to={initURL}/> );
      }
    }

    const currentAppLocale = AppLocale[locale.locale];
    return (
      <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}>

          <Switch>
            <Route exact path='/signin' component={SignIn}/>
            <Route exact path='/signup' component={SignUp}/>
            <Route exact path='/denied' component={Denied}/>
            <RestrictedRoute path={`${match.url}`} isLogged={isLogged}
                             authorized={authorized} component={MainApp}/>
          </Switch>
        </IntlProvider>
      </LocaleProvider>
    )
  }
}

const mapStateToProps = ({settings, auth, authentication}) => {
  const {locale, initURL} = settings;
  const { loadingCheck, isLogged, authorized } = authentication;
  return {locale, isLogged, initURL, loadingCheck, authorized}
};

const mapDispatchToProps = dispatch => {
  return {
    getRefreshToken: (refreshToken) =>  dispatch(loginCheckRequest(refreshToken)),
    setInitUrl: (pathname) => dispatch(setInitUrl(pathname))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
