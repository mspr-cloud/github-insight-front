import React from "react";
import { Button, Divider } from "antd";
import {connect} from "react-redux";
import { injectIntl } from 'react-intl'
import { getAccountRequest, logoutRequest } from 'store/ducks/Authentication';
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

class Denied extends React.Component {

  render() {

    const { token, refresh, disconnect, loading } = this.props;

    return (
      <div className="gi-app-denied-wrap">
        <div className="gi-app-denied-container">
          <div className="gi-app-denied-main-content">
            <div className="gi-app-logo-content">
              <div className="gi-app-logo-content-bg">
              </div>
              <div className="gi-app-logo-wid">
                <h1><IntlMessages id="app.denied.title"/></h1>
              </div>
            </div>
            <div className="gi-app-denied-content">
              <h2><IntlMessages id="app.denied.subTitle"/></h2>
              <p><IntlMessages id="app.denied.firstParagraph"/></p>
              <Divider/>
              <p><IntlMessages id="app.denied.secondParagraph"/></p>
              <div className="gi-flex-row gi-align-items-center gi-justify-content-between gi-flex-1 gi-flex-nowrap">
                <Button type="primary" className="gi-mb-0" onClick={() => refresh(token)}>
                  <IntlMessages id="app.denied.refresh"/>
                </Button>
                <Button type="secondary" className="gi-mb-0" onClick={() => disconnect()}>
                  <IntlMessages id="app.userAuth.logout"/>
                </Button>
              </div>
            </div>

            {loading ?
              <div className="gi-loader-view">
                <CircularProgress/>
              </div> : null}
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  token: state.authentication.token,
  loading: state.authentication.loading
});


const mapDispatchToProps = dispatch => ({
  refresh: (token) => dispatch(getAccountRequest(token)),
  disconnect: () => dispatch(logoutRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Denied));
